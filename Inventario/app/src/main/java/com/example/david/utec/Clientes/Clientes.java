package com.example.david.utec.Clientes;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.SpinnerObjectString;
import com.example.david.utec.Principal.menu;

import com.example.david.utec.Proveedor.RowReporteA;
import com.example.david.utec.Proveedor.modificarElimiarProveedor;
import com.example.david.utec.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Clientes extends FragmentActivity {

    public SQLiteDatabase conexOpen;
    public Context contexto=this;
    public GestionDB objGestionDB;
    public GestionDB obj;

    Dialog customDialog=null;


    public Context context = this;

    String nombreusuario, id_sexo, id_estado;

    EditText e_nombres_cliente, e_apellidos_cliente, e_telefono_cliente, e_direccion_cliente, e_codigo_cliente;
    Spinner sp_sexo, sp_estado;


    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<Tablaclientes> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_clientes);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_clientes);


        Bundle bundle = getIntent().getExtras();
        nombreusuario=bundle.getString("var_user");

      //  Toast.makeText(this,""+nombreusuario,Toast.LENGTH_SHORT).show();

        e_nombres_cliente=(EditText)findViewById(R.id.editTextnombrescliente);
        e_apellidos_cliente=(EditText)findViewById(R.id.editTextApellidoscliente);
        e_telefono_cliente=(EditText)findViewById(R.id.editTexttelefonocliente);
        e_direccion_cliente=(EditText)findViewById(R.id.editTextDireccioncliente);
        e_codigo_cliente=(EditText)findViewById(R.id.editTextcodigocliente);

        sp_sexo=(Spinner)findViewById(R.id.spinnerSexcliente);
        sp_estado=(Spinner)findViewById(R.id.spinnerEstadocliente);

        mTableLayoutReporte=				(TableLayout)findViewById(R.id.tablelayout_clientes);

        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CrearConsulta();

        cargarspinersexo();
        cargarspinerestado();


        // SACAR ID DEL COMBO SEXO SELECCIONADO
        sp_sexo.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_sexo = (((SpinnerObjectString) sp_sexo.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });




        // SACAR ID DEL COMBO SEXO SELECCIONADO
        sp_estado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_estado = (((SpinnerObjectString) sp_estado.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });






    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.generartablacliente(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (Tablaclientes fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombreCompleto      = new TextView(context);
            TextView textViewSexo       = new TextView(context);
            TextView textViewAlias      = new TextView(context);
            TextView textViewDireccion  = new TextView(context);
            TextView textViewTelefono   = new TextView(context);
            TextView textViewEstado     = new TextView(context);
            TextView id                 = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewNombreCompleto.setText((fila.getnombre()));
            textViewSexo.setText((fila.getsexo()));
            textViewAlias.setText(fila.getcodigo());
            textViewDireccion.setText(fila.getdireccion());
            textViewTelefono.setText(fila.gettelefono());
            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            /*
               COLUMNA NOMBRE COMPLETO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreCompleto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreCompleto.setGravity(Gravity.CENTER);
            textViewNombreCompleto.setTextSize(15);
            textViewNombreCompleto.setTypeface(null, Typeface.BOLD);
            textViewNombreCompleto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreCompletoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreCompletoTableRowParams.setMargins(1, 1, 1, 1);
            nombreCompletoTableRowParams.width = 199;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreCompleto, nombreCompletoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA SEXO
                     */
            //Asignamos el color
            textViewSexo.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewSexo.setGravity(Gravity.CENTER);
            textViewSexo.setTextSize(15);
            textViewSexo.setTypeface(null, Typeface.BOLD);
            textViewSexo.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams SexoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            SexoTableRowParams.setMargins(1, 1, 1, 1);
            SexoTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewSexo, SexoTableRowParams);

            //FIN COLUMNA SEXO.

                /*
                COLUMNA ALIAS
                 */
            textViewAlias.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewAlias.setGravity(Gravity.CENTER);
            textViewAlias.setTextSize(15);
            textViewAlias.setTypeface(null, Typeface.BOLD);
            textViewAlias.setHeight(50);

            TableRow.LayoutParams AliasTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            AliasTableRowParams.setMargins(1, 1, 1, 1);
            AliasTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewAlias, AliasTableRowParams);

            // FIN COLUMNA ALIAS


            /*
            COLUMNA DIRECCION
             */
            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewDireccion.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewDireccion.setGravity(Gravity.CENTER);
            textViewDireccion.setTextSize(15);
            textViewDireccion.setTypeface(null, Typeface.BOLD);
            textViewDireccion.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams direccionTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            direccionTableRowParams.setMargins(1, 1, 1, 1);
            direccionTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewDireccion, direccionTableRowParams);
            // FIN COLUMNA DIRECCION


            /*
            columna telefono
             */
            textViewTelefono.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewTelefono.setGravity(Gravity.CENTER);
            textViewTelefono.setTextSize(15);
            textViewTelefono.setTypeface(null, Typeface.BOLD);
            textViewTelefono.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams telefonoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            telefonoTableRowParams.setMargins(1, 1, 1, 1);
            telefonoTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewTelefono, telefonoTableRowParams);
            // fin columna telefono.




            /*
            columna  ESTADO.
             */
            textViewEstado.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewEstado.setGravity(Gravity.CENTER);
            textViewEstado.setTextSize(15);
            textViewEstado.setTypeface(null, Typeface.BOLD);
            textViewEstado.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewEstado, estadoTableRowParams);
            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));






            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                    // Toast.makeText(this, "---->"+s_id , Toast.LENGTH_LONG).show();
                    Intent i = new Intent(Clientes.this, Modificar_Eliminar_Cliente.class);
                    i.putExtra("s_id", s_id);
                    i.putExtra("var_user",nombreusuario);
                    // finish();
                    startActivity(i);


                    // modificarEliminar(s_id);



                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }



    public void cargarspinersexo(){
        // CARGAR COMBO DE SEXO --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getSexo(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_sexo.setAdapter(dataAdapter);



    }

    public void cargarspinerestado(){
        // CARGAR COMBO DE ESTADO --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getEstado(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_estado.setAdapter(dataAdapter);



    }



    public void click_guardar_cliente(View guardarcliente){

        String s_nombres_cliente = e_nombres_cliente.getText().toString().trim();
        String s_apellidos_clientes= e_apellidos_cliente.getText().toString().trim();
        String s_telefono_cliente= e_telefono_cliente.getText().toString().trim();
        String s_direccion_cliente=e_direccion_cliente.getText().toString().trim();
        String s_codigo_cliente=e_codigo_cliente.getText().toString().trim();


     int respuesta= validar(s_nombres_cliente, s_apellidos_clientes, s_telefono_cliente, s_direccion_cliente, s_codigo_cliente, id_sexo, id_estado);

        if(respuesta==0){
            //Toast.makeText(this,"bien",Toast.LENGTH_SHORT).show();

            objGestionDB.ingresarcliente(contexto,s_nombres_cliente, s_apellidos_clientes, s_telefono_cliente, s_direccion_cliente, s_codigo_cliente, id_sexo, id_estado);
                confirmacion();

        }else {
            errorcompletarcampo();
        }
    }

    public int validar(String s_nombres_cliente, String s_apellidos_clientes, String s_telefono_cliente, String s_direccion_cliente,String s_codigo_cliente, String id_sexo,String id_estado){
        int respuesta=0;

        // NOMBRE
        if (s_nombres_cliente.equals("")){

            respuesta=1;

        }

        //APELLIDO
        if (s_apellidos_clientes.equals("")){

            respuesta=1;

        }
        // TELEFONO
        if (s_telefono_cliente.equals("")){

            respuesta=1;

        }
        // DIRECCION
        if (s_direccion_cliente.equals("")){

            respuesta=1;

        }

        // SEXO
        if (id_sexo.equals("0")){

            respuesta=1;

        }

        // ESTADO
        if (id_estado.equals("0")){

            respuesta=1;

        }

        // CODIGO
        if (s_codigo_cliente.equals("")){

            respuesta=1;

        }
        return respuesta;
    }

    public void  click_buscar_cliente(View buscarcliente){

        String s_nombres_cliente = e_nombres_cliente.getText().toString().trim();
        String s_apellidos_clientes= e_apellidos_cliente.getText().toString().trim();
        String s_telefono_cliente= e_telefono_cliente.getText().toString().trim();
        String s_direccion_cliente=e_direccion_cliente.getText().toString().trim();
        String s_codigo_cliente=e_codigo_cliente.getText().toString().trim();


        int respuesta= validarbusqueda(s_nombres_cliente, s_apellidos_clientes, s_telefono_cliente, s_direccion_cliente, s_codigo_cliente, id_sexo, id_estado);

        if(respuesta==0){

           // Toast.makeText(this,"bien", Toast.LENGTH_LONG).show();
            mTableLayoutReporte.removeAllViews();
            mListaActividades=   objGestionDB.generarbusquedatablacliente(contexto, s_nombres_cliente, s_apellidos_clientes, s_telefono_cliente, s_direccion_cliente, s_codigo_cliente, id_sexo, id_estado);

            crearTabla();
        }else{

            informar();

        }


    }

    public int validarbusqueda(String s_nombres_cliente, String s_apellidos_clientes, String s_telefono_cliente, String s_direccion_cliente,String s_codigo_cliente, String id_sexo,String id_estado){
        int respuesta=0;


        if  (   (s_nombres_cliente.equals(""))     &&      (s_apellidos_clientes.equals(""))    &&      (s_telefono_cliente.equals(""))  &&      (s_direccion_cliente.equals(""))   &&      (s_codigo_cliente.equals(""))   &&      (id_sexo.equals("0"))  &&      (id_estado.equals("0"))        ){
            respuesta=1;
        }

        return  respuesta;
    }


    public void regresaralmenu(View regresar){

        Intent i = new Intent(Clientes.this, menu.class);
        i.putExtra("var_user", nombreusuario);
        finish();
        startActivity(i);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_clientes, menu);
        return true;
    }




    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Debe de Ingresar al menos un Parámetro, Para Realizar la Búsqueda..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


        // COMPLETAR CAMPOS

    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });

	    /*((Button) customDialog.findViewById(R.id.cancelar)).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View view)
			{
				customDialog.dismiss();
				//Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

			}
		});*/

        customDialog.show();
    }


    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Cliente Ingresado Correctamente..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Clientes.this,Clientes.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
