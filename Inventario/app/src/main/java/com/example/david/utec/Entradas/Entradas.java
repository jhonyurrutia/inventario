package com.example.david.utec.Entradas;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.support.v4.app.FragmentManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;



import com.example.david.utec.Principal.*;
import com.example.david.utec.Entradas.*;


import android.support.v4.app.Fragment;


import com.example.david.utec.Proveedor.RowReporteA;
import com.example.david.utec.Proveedor.modificarElimiarProveedor;
import com.example.david.utec.R;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

//public class Entradas extends Activity implements OnClickListener {
public class Entradas extends Activity implements OnClickListener{

    public SQLiteDatabase conxOpen;
    public Context contexto= this;
    public Context context = this;
    public GestionDB objGestionDB;
    public  GestionDB obj;

    Dialog customDialog= null;
    String nombreusuario, id_proveedor, id_producto;
    Spinner sp_proveedor, sp_producto;
    private TextView cantidad_producto, fecha_vencimiento;
    public ImageButton ibutton_fecha_vencimiento;

    String fecha;
    int lote;

    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<TablaEntradas> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_entradas);

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
        this.setContentView(R.layout.activity_entradas);


        Bundle bundle= getIntent().getExtras();
       nombreusuario= bundle.getString("var_user");

       // Toast.makeText(this,""+nombreusuario,Toast.LENGTH_SHORT).show();



        ibutton_fecha_vencimiento = (ImageButton) findViewById(R.id.imageFechavencimiento);
        fecha_vencimiento = (EditText) findViewById(R.id.txt_fecha_vencimiento);
        cantidad_producto= (EditText)findViewById(R.id.editTextcantidadproducto);
        ibutton_fecha_vencimiento.setOnClickListener(this);
       // fecha_nacimiento.setFocusable(false);

        ibutton_fecha_vencimiento = (ImageButton) findViewById(R.id.imageFechavencimiento);

        sp_proveedor=(Spinner)findViewById(R.id.spinnerproveedorentradas);
        sp_producto=(Spinner)findViewById(R.id.spinnerproductoentradas);


        mTableLayoutReporte=				(TableLayout)findViewById(R.id.tablelayout_entradas);






        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        CrearConsulta();
        cargarspinerproveedor();
        objGestionDB.consultarfecha(contexto);
        objGestionDB.consultarultimolote(contexto);

        fecha=(objGestionDB.fecha);
        lote=(objGestionDB.lote);

        lote=lote+1;



      //  Toast.makeText(this,"lote"+lote,Toast.LENGTH_SHORT).show();


        // SACAR ID DEL COMBO proveedor SELECCIONADO
        sp_proveedor.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_proveedor = (((SpinnerObjectString) sp_proveedor.getSelectedItem()).getCodigo());
                cargarspinerproducto(id_proveedor);


            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });


            // sacar id producto
        sp_producto.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_producto = (((SpinnerObjectString) sp_producto.getSelectedItem()).getCodigo());
                //loadSpinnerDataSp_municipio(id_depto);// llamo al metodo que va a cargar los municipios
            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }
        });


    }

    public void CrearConsulta(){

        mListaActividades=objGestionDB.consultaentradas(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (TablaEntradas fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewNombreCompleto      = new TextView(context);
            TextView textViewSexo       = new TextView(context);
            TextView textViewAlias      = new TextView(context);
            TextView textViewDireccion  = new TextView(context);
            TextView textViewTelefono   = new TextView(context);
            TextView textViewEstado     = new TextView(context);
            TextView id                 = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewNombreCompleto.setText((fila.getproveedor()));
            textViewSexo.setText((fila.getproducto()));
            textViewAlias.setText(fila.getcantidad_producto());
            textViewDireccion.setText(fila.getfechavencimiento());
            textViewTelefono.setText(fila.getfecha());
            textViewEstado.setText(fila.getestado());
            id.setText((fila.getid()));

            /*
               COLUMNA NOMBRE COMPLETO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreCompleto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreCompleto.setGravity(Gravity.CENTER);
            textViewNombreCompleto.setTextSize(15);
            textViewNombreCompleto.setTypeface(null, Typeface.BOLD);
            textViewNombreCompleto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreCompletoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreCompletoTableRowParams.setMargins(1, 1, 1, 1);
            nombreCompletoTableRowParams.width = 99;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreCompleto, nombreCompletoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA SEXO
                     */
            //Asignamos el color
            textViewSexo.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewSexo.setGravity(Gravity.CENTER);
            textViewSexo.setTextSize(15);
            textViewSexo.setTypeface(null, Typeface.BOLD);
            textViewSexo.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams SexoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            SexoTableRowParams.setMargins(1, 1, 1, 1);
            SexoTableRowParams.width = 200;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewSexo, SexoTableRowParams);

            //FIN COLUMNA SEXO.

                /*
                COLUMNA ALIAS
                 */
            textViewAlias.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewAlias.setGravity(Gravity.CENTER);
            textViewAlias.setTextSize(15);
            textViewAlias.setTypeface(null, Typeface.BOLD);
            textViewAlias.setHeight(50);

            TableRow.LayoutParams AliasTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            AliasTableRowParams.setMargins(1, 1, 1, 1);
            AliasTableRowParams.width = 100;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewAlias, AliasTableRowParams);

            // FIN COLUMNA ALIAS


            /*
            COLUMNA DIRECCION
             */
            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewDireccion.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewDireccion.setGravity(Gravity.CENTER);
            textViewDireccion.setTextSize(15);
            textViewDireccion.setTypeface(null, Typeface.BOLD);
            textViewDireccion.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams direccionTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            direccionTableRowParams.setMargins(1, 1, 1, 1);
            direccionTableRowParams.width = 210;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewDireccion, direccionTableRowParams);
            // FIN COLUMNA DIRECCION


            /*
            columna telefono
             */
           textViewTelefono.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewTelefono.setGravity(Gravity.CENTER);
            textViewTelefono.setTextSize(15);
            textViewTelefono.setTypeface(null, Typeface.BOLD);
            textViewTelefono.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams telefonoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            telefonoTableRowParams.setMargins(1, 1, 1, 1);
            telefonoTableRowParams.width = 110;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewTelefono, telefonoTableRowParams);
            // fin columna telefono.




            /*
            columna  ESTADO.
             */
            textViewEstado.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewEstado.setGravity(Gravity.CENTER);
            textViewEstado.setTextSize(15);
            textViewEstado.setTypeface(null, Typeface.BOLD);
            textViewEstado.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams estadoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            estadoTableRowParams.setMargins(1, 1, 1, 1);
            estadoTableRowParams.width = 120;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewEstado, estadoTableRowParams);

            // columna estado.




            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));











            //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v)
                {


                    tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));
                    // Toast.makeText(this, "---->"+s_id , Toast.LENGTH_LONG).show();
                   Intent i = new Intent(Entradas.this, modificar_eliminar_entradas.class);
                    i.putExtra("s_id", s_id);
                    i.putExtra("var_user",nombreusuario);
                    // finish();
                    startActivity(i);


                    // modificarEliminar(s_id);



                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }


    public void cargarspinerproveedor(){
        // CARGAR COMBO PROVEEDOR --> SPINNER

        List<SpinnerObjectString> lables = objGestionDB.getProveedor(this.contexto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_proveedor.setAdapter(dataAdapter);



    }

    public void cargarspinerproducto(String id_proveedor){
        // CARGAR COMBO PRODUCTO --> SPINNER

     //   Toast.makeText(this,""+id_proveedor,Toast.LENGTH_SHORT).show();

        List<SpinnerObjectString> lables = objGestionDB.getProducto(this.contexto, id_proveedor);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_producto.setAdapter(dataAdapter);



    }


    public void click_guardar_entrada (View guardar){


        String  s_cantidad_producto=   cantidad_producto.getText().toString().trim();
        String s_fecha_vencimiento= fecha_vencimiento.getText().toString().trim();

      int respuesta=  validar(id_proveedor, id_producto, s_cantidad_producto, s_fecha_vencimiento);

        if(respuesta==0){

            //Toast.makeText(this,"bien",Toast.LENGTH_SHORT).show();


            objGestionDB.agregarentradas(contexto,id_proveedor, id_producto, s_cantidad_producto, s_fecha_vencimiento,fecha, lote);
            confirmacion();



        }else{
           // Toast.makeText(this,"mal", Toast.LENGTH_SHORT).show();

            errorcompletarcampo();
        }

    }


    public void click_BuscarEntradas( View buscar ){

        String  s_cantidad_producto=   cantidad_producto.getText().toString().trim();
        String s_fecha_vencimiento= fecha_vencimiento.getText().toString().trim();

        int respuesta=  validarbusqueda(id_proveedor, id_producto, s_cantidad_producto, s_fecha_vencimiento);

        if(respuesta==0){

            mTableLayoutReporte.removeAllViews();
            mListaActividades=objGestionDB.consultaentradasbusqueda(contexto, id_proveedor, id_producto, s_cantidad_producto, s_fecha_vencimiento);
            crearTabla();

            //Toast.makeText(this,"ok buscar", Toast.LENGTH_SHORT).show();


        }else{

            informar();
        }

    }

    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Debe de Ingresar al menos un Parámetro, Para Realizar la Búsqueda..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }


    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("La Entrada al Producto a sido Ingresada Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Entradas.this,Entradas.class);
                i.putExtra("var_user",nombreusuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }

    public int validar(String id_proveedor, String id_producto, String s_cantidad_producto, String s_fecha_vencimiento ){
        int respuesta=0;

        if (id_producto.equals("0")){
            respuesta=1;

        }

        if(id_proveedor.equals("0")){
            respuesta=1;
        }

        if(s_cantidad_producto.equals("")){
            respuesta=1;

        }
        if(s_fecha_vencimiento.equals("")){
            respuesta=1;

        }

        return respuesta;
    }

    public int validarbusqueda(String id_proveedor, String id_producto, String s_cantidad_producto, String s_fecha_vencimiento ){
        int respuesta=0;

        if (  ( id_producto.equals("0"))  &&  (id_proveedor.equals("0")) &&  (s_cantidad_producto.equals(""))  &&(s_fecha_vencimiento.equals(""))  ){
            respuesta=1;

        }





        return respuesta;
    }


    public void regresarentradasmenu(View regresar){

        Intent i= new Intent(this, menu.class);
        i.putExtra("var_user",nombreusuario);
        finish();
        startActivity(i);

    }




    public void onClick(View v){
        showDialog(0);
    }
    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id){
        Calendar fechaActual = new GregorianCalendar();
        int anioActual = fechaActual.get(Calendar.YEAR);
        int mesoActual = fechaActual.get(Calendar.MONTH);
        int diaActual  = fechaActual.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(this, datePickerListener, anioActual, mesoActual, diaActual);
    }
    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,int selectedMonth, int selectedDay) {


            //fechaActual;
            Calendar fechaActual = new GregorianCalendar();
            int anioActual = fechaActual.get(Calendar.YEAR);
            int mesoActual = fechaActual.get(Calendar.MONTH);
            int diaActual  = fechaActual.get(Calendar.DAY_OF_MONTH);

            String fechita= selectedYear+"-"+(selectedMonth + 1)+"-"+selectedDay;
            fecha_vencimiento.setText(fechita);





        }
    };






}
