/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.example.david.utec.Clientes;

import java.util.ArrayList;

public class Tablaclientes {
	public String nombre;						                        //Esta cadena representa la fecha en que debe de realizarse las actividades
	public String sexo;
	public String codigo;
	public String direccion;
	public String telefono;
	public String estado;
	public String id;
	public ArrayList<String> Actividades = new ArrayList<String>();		//Esta es la lista de todas las actividades que se realizan es la misma fecha


	// NOMBRE

	public String getnombre() {
		return nombre;
	}
	public void setnombre(String nombre) {
		nombre = nombre;
	}


		// SEXO
	public String getsexo() {
		return sexo;
	}
	public void setsexo(String sexo) {
		sexo = sexo;
	}


	// ESTADO
	public String getestado() {
		return estado;
	}
	public void setestado(String estado) {
		estado = estado;
	}

		// Codigo
	public String getcodigo() {
		return codigo;
	}
	public void setcodigo(String codigo) {
		codigo = codigo;
	}



	// DIRECCION
	public String getdireccion() {
		return direccion;
	}
	public void setdireccion(String direccion) {
		direccion = direccion;
	}

	// TELEFONO
	public String gettelefono() {
		return telefono;
	}
	public void settelefono(String telefono) {
		telefono = telefono;
	}

	// ID_PROVEEDOR
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	

	/*public ArrayList<String> getActividades() {
		return Actividades;
	}
	public void setActividades(ArrayList<String> actividades) {
		Actividades = actividades;
	}*/
	
	

}
