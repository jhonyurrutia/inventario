/**
 *Esta clase representa los datos dde una fila
 *de la tabla de actividades del reporte A 
 */
package com.example.david.utec.Salidas;

import java.util.ArrayList;

public class ReporteSalidas {
	public String cliente;
	public String proveedor;
	public String nombre_producto;						                        //Esta cadena representa la fecha en que debe de realizarse las actividades
	public String cantidad;
	public String lote;
	public String fechavencimiento;
	public String id;
	public ArrayList<String> Actividades = new ArrayList<String>();		//Esta es la lista de todas las actividades que se realizan es la misma fecha


	// NOMBRE del CLIENTE

	public String getcliente() {
		return cliente;
	}
	public void setcliente(String cliente) {
		cliente = cliente;
	}


	// NOMBRE DEL PROVEEDOR
	public String getproveedor() {
		return proveedor;
	}
	public void setproveedor(String proveedor) {
		proveedor = proveedor;
	}



	// NOMBRE del producto

	public String getnombre() {
		return nombre_producto;
	}
	public void setnombre(String nombre_producto) {
		nombre_producto = nombre_producto;
	}


		// cantidad
	public String getcantidad() {
		return cantidad;
	}
	public void setcantidad(String cantidad) {
		cantidad = cantidad;
	}


	// lote
	public String getlote() {
		return lote;
	}
	public void setlote(String lote) {
		lote = lote;
	}

		// fecha vencimiento
	public String getfechavencimiento() {
		return fechavencimiento;
	}
	public void setfechavencimiento(String fechavencimiento) {
		fechavencimiento = fechavencimiento;
	}





	// id detalle
	public String getid() {
		return id;
	}
	public void setid(String id) {
		id = id;
	}
	


	

}
