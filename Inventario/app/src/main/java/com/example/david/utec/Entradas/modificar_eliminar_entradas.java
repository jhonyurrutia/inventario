package com.example.david.utec.Entradas;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Producto.Modificar_Producto;
import com.example.david.utec.Producto.Producto;
import com.example.david.utec.R;

import java.io.IOException;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class modificar_eliminar_entradas extends Activity {

    String id_entrada, nombre_usuario;


    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;


    Dialog customDialog = null;

    public Context contexto = this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       // setContentView(R.layout.activity_modificar_eliminar_entradas);

        Bundle bundle= getIntent().getExtras();
        id_entrada= bundle.getString("s_id");
        nombre_usuario= bundle.getString("var_user");

      //  Toast.makeText(this,"-->"+id_entrada+nombre_usuario,Toast.LENGTH_SHORT).show();


        BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.modificar_elimar);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloModificarEliminar);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeModificarElimar);
        contenido.setText("¿Modificar o Eliminar?");



        ((ImageView) customDialog.findViewById(R.id.ima_cerrar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

                Intent i = new Intent(modificar_eliminar_entradas.this, Entradas.class);
                i.putExtra("var_user",nombre_usuario);
                finish();
                startActivity(i);

            }
        });
        // EDITAR
        ((ImageView) customDialog.findViewById(R.id.img_editar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //  customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                Intent i = new Intent(modificar_eliminar_entradas.this, Modificar_Entradas.class);
                i.putExtra("s_id",id_entrada);
                i.putExtra("var_user",nombre_usuario);
                finish();
                startActivity(i);

                //  Toast.makeText(this, "id-->"+s_id, Toast.LENGTH_LONG).show();
            }
        });
        //ELIMINAR
        ((ImageView) customDialog.findViewById(R.id.img_eliminar)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

                eliminar( id_entrada);

                //  Toast.makeText(this, "eliminar", Toast.LENGTH_LONG).show();
            }
        });

        customDialog.show();



    }


    public void eliminar(final String id_entrada){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Eliminar Entrada");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Una vez que elimines la Entrada, no podras deshacer la acción");

        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(modificar_eliminar_entradas.this,Entradas.class);
                i.putExtra("var_user",nombre_usuario);
                finish();
                startActivity(i);

            }
        });

        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // customDialog.dismiss();




                objGestionDB.eliminarEntrada(contexto, id_entrada);
                confirmarEliminacion();



            }
        });



        customDialog.show();

    }


    public void  confirmarEliminacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Entrada Eliminada Correctamente..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(modificar_eliminar_entradas.this,Entradas.class);
                i.putExtra("var_user",nombre_usuario);
                finish();
                startActivity(i);

            }
        });



        customDialog.show();

    }

}
