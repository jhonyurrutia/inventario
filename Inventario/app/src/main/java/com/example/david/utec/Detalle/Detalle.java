package com.example.david.utec.Detalle;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Typeface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.david.utec.Principal.SpinnerObjectString;
import com.example.david.utec.Producto.Producto;
import com.example.david.utec.Producto.ReporteProductos;
import com.example.david.utec.Producto.modificarEliminarProducto;
import com.example.david.utec.Proveedor.proveedor;
import com.example.david.utec.R;
import com.example.david.utec.Salidas.Salidas;
import com.example.david.utec.Salidas.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import db_gestion.BaseDeDatos;
import db_gestion.GestionDB;

public class Detalle extends Activity {

    String nombreusuario,id_proveedor, id_producto, id_cliente,  id_lote;


    public SQLiteDatabase conexOpen;
    public Context context = this;
    public GestionDB objGestionDB ;
    public GestionDB obj;


    Dialog customDialog = null;

    public Context contexto = this;

    EditText proveedor,  cliente, cantidad;

    Spinner sp_lote;

    TextView  tex_cliente, tex_proveedor,  tex_producto;


    int   id_salida=0;

    // para la tabla
    private TableLayout mTableLayoutReporte;
    private ArrayList<ReporteDetalles> mListaActividades;
    private LinearLayout mLinearLayoutContenedorPrincipal;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
      // setContentView(R.layout.activity_detalle);


        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //Remove notification bar
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        //set content view AFTER ABOVE sequence (to avoid crash)
       // this.setContentView(R.layout.activity_detalle);

        Bundle bundle= getIntent().getExtras();
        nombreusuario=bundle.getString("var_user");
        id_proveedor=bundle.getString("id_proveedor");
        id_producto=bundle.getString("id_producto");
        id_cliente=bundle.getString("idcliente");

       // Toast.makeText(this,nombreusuario+id_proveedor+id_producto+id_cliente,Toast.LENGTH_SHORT).show();



        final BaseDeDatos objBaseDeDatos = new BaseDeDatos(this);
        try {
            objBaseDeDatos.createDataBase();
            // db2.openDataBase();
            //this.conexOpen = db2.myDataBase;
            this.objGestionDB = new GestionDB();// creo el objeto de la clase que gestiona la DB
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }



       // objGestionDB.insertarSalida(contexto, id_cliente);  // insertamos la salida del cliente

          id_salida = objGestionDB.consultarid_salida(contexto);

      //  Toast.makeText(this,"salida"+id_salida,Toast.LENGTH_SHORT).show();



       // proveedor=(EditText) findViewById(R.id.editTextproveedordetalle);

        objGestionDB.detalleProveedor(contexto, id_proveedor);
        objGestionDB.detalleProducto(contexto, id_producto);
        objGestionDB.detalleCliente(contexto, id_cliente);

      //  proveedor.setText(objGestionDB.proveedor_detalle);
        //producto.setText(objGestionDB.producto_detalle);


        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.activity_detalle);







            // cerrar -- regresar a Salidas
        ((ImageView) customDialog.findViewById(R.id.ima_cerrar_detalle)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();

                // eliminar la salida tambien  id_salida
                objGestionDB.eliminarsalida(contexto, id_salida);
                objGestionDB.eliminartodo_detalle(contexto);  // eliminamos reguistros de la tabla temporal si hay

                Intent i = new Intent(Detalle.this, Salidas.class);
                i.putExtra("var_user", nombreusuario);
                finish();
                startActivity(i);

            }
        });


        mTableLayoutReporte=(TableLayout)customDialog.findViewById(R.id.tablelayout_detalle);
        CrearConsulta();

        cantidad=(EditText)customDialog.findViewById(R.id.editTextcantidadproductodetalle);
        // COMBO LOTE

        sp_lote=(Spinner)customDialog.findViewById(R.id.spinnerlotedetalle);



        sp_lote.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            // cuando se ha seleccionado un item del spinner
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                // guardo en variable el id del depto seleccionado
                id_lote = (((SpinnerObjectString) sp_lote.getSelectedItem()).getCodigo());


            }

            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub
            }

        });


             cargarspinnerlote();



        tex_proveedor =(TextView) customDialog.findViewById(R.id.text_proveedordetalle);
        tex_producto =(TextView) customDialog.findViewById(R.id.text_productodetalle);
        tex_cliente=(TextView)customDialog.findViewById(R.id.text_cliente_detalle);
        //text_cliente

        tex_cliente.setText(objGestionDB.cliente_detalle);
        tex_proveedor.setText(objGestionDB.proveedor_detalle);
        tex_producto.setText(objGestionDB.producto_detalle);




        // GUARDAD --> GUARDAR DETALLE
        ((ImageView) customDialog.findViewById(R.id.imageViewguardardetalle)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();


                click_guardardetallesalida();

            }
        });





        // ACEPTAR --> GUARDAR_TODO  EN DETALLE_SALIDA
        ((Button) customDialog.findViewById(R.id.btn_aceptadetalle)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                //customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.cancelar, Toast.LENGTH_SHORT).show();




                // ver si hay registros si  hay guardar sino
                int cantidad_registrostemporales=objGestionDB.consultarregistrostemporales(contexto,id_salida);

                if (cantidad_registrostemporales>0){

                   // guardartodo();
                    cambiar_estado();

                }else{

                    informarsindetalles();

                }



                // else decirle que no hay nada que guardar.

            }
        });


        // CANCELAR_TODO
        ((Button) customDialog.findViewById(R.id.btn_cancelardetalle)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                // ver si hay registros si  hay guardar sino
                int cantidad_registrostemporales=objGestionDB.consultarregistrostemporales(contexto,id_salida);

                if (cantidad_registrostemporales>0){
                    cancelardetalle();
                }else{

                    objGestionDB.eliminarsalida(contexto, id_salida);

                    objGestionDB.eliminartodo_detalle(contexto);  // eliminamos reguistros de la tabla temporal si an quedado ahi.

                    Intent i = new Intent(Detalle.this, Salidas.class);
                    i.putExtra("var_user", nombreusuario);
                    finish();
                    startActivity(i);
                }




            }
        });


        customDialog.show();
    }


    public void CrearConsulta(){

        mListaActividades=objGestionDB.generartabladetalle(contexto);
        crearTabla();
    }

    private void crearTabla(){

        //Removemos el contenido anterior de la tabla en caso de que exista
        mTableLayoutReporte.removeAllViews();  //removeAllViewsInLayout ()
        mTableLayoutReporte.removeAllViewsInLayout();
        //Colocamo el nombre de la seleccion
        // mTextViewNombreSeleccion.setText(mNombreSeleccion)
//;
        for (ReporteDetalles fila : mListaActividades) {

            //Creamos una nueva instancica de fila para la tabla
            final TableRow tableRow = new TableRow(context);
            // ListView listview = new ListView(context);

            //  tableRow.width = 252;

            //Creamos el texview donde se mostrara la informacion
            TextView textViewcliente        = new TextView(context);
            TextView textViewNombreproduto  = new TextView(context);
            TextView textViewcantidad       = new TextView(context);
            TextView textViewlote           = new TextView(context);
            TextView textViewvencimiento    = new TextView(context);
            TextView id                     = new TextView(context);


            //Colocamos la nombre a mostrar en la columna
            textViewcliente.setText((fila.getcliente()));
            textViewNombreproduto.setText((fila.getnombre()));
            textViewcantidad.setText((fila.getcantidad()));
            textViewlote.setText(fila.getlote());

            textViewvencimiento.setText(fila.getfechavencimiento());
            id.setText((fila.getid()));



             /*
               COLUMNA NOMBRE cliente
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewcliente.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewcliente.setGravity(Gravity.CENTER);
            textViewcliente.setTextSize(15);
            textViewcliente.setTypeface(null, Typeface.BOLD);
            textViewcliente.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreclienteTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreclienteTableRowParams.setMargins(1, 1, 1, 1);
            nombreclienteTableRowParams.width = 197;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcliente, nombreclienteTableRowParams);

            // FIN COLUMNA NOMBRE cliente


            /*
               COLUMNA NOMBRE PRODUCTO
            */

            //Asignamos el color de backgroud que tendra la primer columna de la tabla
            textViewNombreproduto.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewNombreproduto.setGravity(Gravity.CENTER);
            textViewNombreproduto.setTextSize(15);
            textViewNombreproduto.setTypeface(null, Typeface.BOLD);
            textViewNombreproduto.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams nombreproductoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            nombreproductoTableRowParams.setMargins(1, 1, 1, 1);
            nombreproductoTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewNombreproduto, nombreproductoTableRowParams);

            // FIN COLUMNA NOMBRE


                    /*
                    COLUMNA CANTIDAD
                     */
            //Asignamos el color
            textViewcantidad.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewcantidad.setGravity(Gravity.CENTER);
            textViewcantidad.setTextSize(15);
            textViewcantidad.setTypeface(null, Typeface.BOLD);
            textViewcantidad.setHeight(50);


            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams cantidadTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            cantidadTableRowParams.setMargins(1, 1, 1, 1);
            cantidadTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewcantidad, cantidadTableRowParams);

            //FIN COLUMNA CODIGO.

                /*
                COLUMNA LOTE
                 */
            textViewlote.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));


            textViewlote.setGravity(Gravity.CENTER);
            textViewlote.setTextSize(15);
            textViewlote.setTypeface(null, Typeface.BOLD);
            textViewlote.setHeight(50);

            TableRow.LayoutParams loteTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            loteTableRowParams.setMargins(1, 1, 1, 1);
            loteTableRowParams.width = 102;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewlote, loteTableRowParams);

            // FIN COLUMNA LOTE

             /*
            columna  FECHA VENCIMIENTO.
             */
            textViewvencimiento.setBackgroundColor(getResources().getColor(R.color.color_dialogo_background));

            //Colocamos la orientacion del texto, tama�o, el estilo negritas y la altura del view
            textViewvencimiento.setGravity(Gravity.CENTER);
            textViewvencimiento.setTextSize(15);
            textViewvencimiento.setTypeface(null, Typeface.BOLD);
            textViewvencimiento.setHeight(50);

            //Creamos los parametros de layout para la primera columna de la tabla
            TableRow.LayoutParams vencimientoTableRowParams = new TableRow.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.MATCH_PARENT);
            vencimientoTableRowParams.setMargins(1, 1, 1, 1);
            vencimientoTableRowParams.width = 130;

            //Agregamos el textview de la fecha a la tabla para que ocupe la primera columna
            tableRow.addView(textViewvencimiento, vencimientoTableRowParams);
            // columna estado.

            //Creamos el linear layout que contendra las actividades de la fecha
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            layout.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));

             //Agregamos las actividades a la fila de la tabla para que utilicen la segunda columna
            // tableRow.addView(layout, activdadTableRowParams);
            final String s_id 	    = id.getText().toString().trim();
            tableRow.setOnClickListener(new View.OnClickListener()
            {
                @Override
                public void onClick(View v) {

            tableRow.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_light));

                    Intent i = new Intent(Detalle.this, ModificarEliminarDetalle.class);
                    i.putExtra("s_id",s_id);
                    i.putExtra("var_user" ,nombreusuario);
                    i.putExtra("id_proveedor" ,id_proveedor);
                    i.putExtra("id_producto" ,id_producto);
                    i.putExtra("id_cliente" ,id_cliente);
                    startActivity(i);
                }
            });

            //Agregamos la fila creada a la tabla de la pantalla

            mTableLayoutReporte.addView(tableRow);

        }

    }


    public void click_guardardetallesalida(){

       String s_cantidad = cantidad.getText().toString().trim();

      int respuesta=  validar(s_cantidad, id_lote);
          if(respuesta==0){

                            int existencia= objGestionDB.vercantidad(contexto, id_producto, id_lote);
                            // Toast.makeText(this,"esto-->"+existencia,Toast.LENGTH_SHORT).show();
                            int cantidad = Integer.parseInt(s_cantidad);

                                        if (cantidad<=existencia){

                                        // Toast.makeText(this,"bien"+id_lote+"cantidad-->"+s_cantidad+id_producto,Toast.LENGTH_SHORT).show();

                                      // int   id_salida = objGestionDB.consultarid_salida(contexto);

                                            objGestionDB.insertardetalletemporal(contexto, id_salida, id_lote, s_cantidad, id_producto);

                                           // CrearConsulta();
                                            confirmacion();

                                            // Toast.makeText(this,"id_salida"+id_salida,Toast.LENGTH_SHORT).show();

                                        }else{
                                            informar();
                                            //Toast.makeText(this,"cantidad seleccionada es mayor a la cantidad con la que el lote dispone-->"+existencia,Toast.LENGTH_SHORT).show();
                                        }

                    }else{
                            errorcompletarcampo();
                        }

       // Toast.makeText(this,s_cantidad+id_lote,Toast.LENGTH_SHORT).show();

    }

    public void guardartodo(){


       // Toast.makeText(this,"guardar todo", Toast.LENGTH_SHORT).show();

        objGestionDB.insertartodoslosdetalles(contexto);
        objGestionDB.eliminartodo_detalle(contexto);

        confirmaciondetalles();


    }


    public void cancelardetalle(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.alerta);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloalerta);
        titulo.setText("Cancelar Detalles");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajealerta);
        contenido.setText("Se Cancelarán Los Detalles Ya Registrados..");


                    // CANCELAR
        ((Button) customDialog.findViewById(R.id.btn_cancelaraceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


            }
        });
            // ACEPTAR
        ((Button) customDialog.findViewById(R.id.btn_aceptarAlerta)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                // customDialog.dismiss();


                // eliminar la salida tambien  id_salida
                objGestionDB.eliminarsalida(contexto, id_salida);

                objGestionDB.eliminartodo_detalle(contexto);  // eliminamos reguistros de la tabla temporal si an quedado ahi.

                Intent i = new Intent(Detalle.this, Salidas.class);
                i.putExtra("var_user", nombreusuario);
                finish();
                startActivity(i);


            }
        });



        customDialog.show();

    }

    public void  confirmaciondetalles(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Detalles Agregados Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);



                Intent i = new Intent(Detalle.this, Salidas.class);
                i.putExtra("var_user", nombreusuario);
                finish();
                startActivity(i);





            }
        });



        customDialog.show();

    }



    public void cambiar_estado(){


        ///// SOLUCIONAR




        int cantidad= objGestionDB.consultarcantidaddeentradasacambiarestado(contexto, id_producto);

        Toast.makeText(this, "Ingresado  cantidad"+cantidad,Toast.LENGTH_SHORT).show();

        if (cantidad==0){

            guardartodo();

        }else{

            int  id_entrada= objGestionDB.consultarentradaid(contexto, id_producto);
            objGestionDB.actualizarestadoentradas(contexto, id_entrada);
            cambiar_estado();
        }








    }



    public void  confirmacion(){

        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.confirmacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloconfirmacion);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeconfirmacion);
        contenido.setText("Detalle Ingresado Correctamente");

        ((Button) customDialog.findViewById(R.id.btn_aceptarConfirmacion)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();
                // i.putExtra("var_user",nombreusuario);


                Intent i= new Intent(Detalle.this,Detalle.class);

                i.putExtra("var_user",nombreusuario);
                i.putExtra("id_proveedor",id_proveedor);
                i.putExtra("id_producto",id_producto);
                i.putExtra("idcliente",id_cliente);
                startActivity(i);
                finish();
                startActivity(i);





            }
        });



        customDialog.show();

    }

    public void informar()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Cantidad seleccionada es mayor a la cantidad con la que el lote dispone..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view)
            {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }



    public void informarsindetalles()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.mensajeinformacion);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("No Hay Detalles Registrados Para Guardar..");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });


        customDialog.show();
    }

    public void errorcompletarcampo()
    {
        // con este tema personalizado evitamos los bordes por defecto
        customDialog = new Dialog(this,R.style.Theme_Dialog_Translucent);
        //deshabilitamos el título por defecto
        customDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        //obligamos al usuario a pulsar los botones para cerrarlo
        customDialog.setCancelable(false);
        //establecemos el contenido de nuestro dialog
        customDialog.setContentView(R.layout.errorcompletecampos);

        TextView titulo = (TextView) customDialog.findViewById(R.id.textview_tituloErrorUsuario);
        titulo.setText("ERROR!!");

        TextView contenido = (TextView) customDialog.findViewById(R.id.textview_mensajeErrorUsuario);
        contenido.setText("Complete los Campos");

        ((Button) customDialog.findViewById(R.id.btn_aceptarErrorUsuario)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                customDialog.dismiss();
                //Toast.makeText(MainActivity.this, R.string.aceptar, Toast.LENGTH_SHORT).show();

            }
        });



        customDialog.show();
    }

    public int  validar (String s_cantidad, String id_lote){

        int respuesta=0;

        if (s_cantidad.equals("")  || id_lote.equals("0")  ){

            respuesta=1;
        }
        return respuesta;
    }




    public void cargarspinnerlote2(){
        // CARGAR COMBO LOTE --> SPINNER

      // Toast.makeText(this, "segunda vez", Toast.LENGTH_SHORT).show();

      int cantidadtemporal=  objGestionDB.vercantidadsegunda(this.contexto, id_producto);

        Toast.makeText(this, "segunda vez"+cantidadtemporal,Toast.LENGTH_SHORT).show();


     /*   List<SpinnerObjectString> lables = objGestionDB.getLotesegundaves(this.contexto, id_producto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_lote.setAdapter(dataAdapter); */

    }






    public void cargarspinnerlote(){
        // CARGAR COMBO LOTE --> SPINNER




        List<SpinnerObjectString> lables = objGestionDB.getLote(this.contexto, id_producto);
        ArrayAdapter<SpinnerObjectString> dataAdapter = new ArrayAdapter<SpinnerObjectString>(this, android.R.layout.simple_spinner_item, lables);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_lote.setAdapter(dataAdapter);

    }

}
