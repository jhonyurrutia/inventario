package db_gestion;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;
import android.widget.Toast;

//import com.example.david.utec.Principal.SpinnerObject;
//import com.example.david.utec.Principal.SpinnerObjectString;

import com.example.david.utec.Clientes.Tablaclientes;
import com.example.david.utec.Principal.*;

import com.example.david.utec.Proveedor.*;
import com.example.david.utec.Producto.*;
import com.example.david.utec.Entradas.*;
import com.example.david.utec.Salidas.*;
import com.example.david.utec.Detalle.*;
import com.example.david.utec.Existencias.*;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;



public class GestionDB {
	
	
	//variable para el login
	public int id_usuario=0;
	public String nombre ="";
	public String password_db ="";

	public String nombreusuario="";



			// fecha actual
	public String fecha="";

	// lote
	public int lote=0;


	// id_salida
	public int id_salida=0;


	// VARIABLES PARA ACTUALIZAR PROVEEDOR
	public String nombres_proveedor="";
	public String apellidos_proveedor="";
	public String alias_proveedor="";
	public String direccion_proveedor="";
	public String telefono_proveedor="";
	public String sexo_proveedor="";
	public int idsexo=0;
	public  int idestado=0;


	// VARIABLES PARA ACTUALIZAR CLIENTE
	public String nombres_cliente="";
	public String apellidos_cliente="";
	public String codigo_cliente="";
	public String direccion_cliente="";
	public String telefono_cliente="";
	public String sexo_cliente="";
	public int idsexo_cliente=0;
	public  int idestado_cliente=0;


	//variables para actualizar producto.
	public String nombre_producto="", codigo_producto="";
	public int id_proveedor=0, id_estado=0;


	// varibales para el detalle
	public String proveedor_detalle="";
	public String producto_detalle="";
	public String cliente_detalle="";


	/// cliente
	public String nombre_cliente="";


	// modificar detalle LOTE , cantidad
	public int id_lotemodificar=0;
	public  String cantidad="";


	// variables para actualizar entradas

	public String fecha_vencimiento="";
	public String  cantidad_producto="";
	public int  id_proveedorentradas=0;
	public int  id_productoentradas=0;


	// varibale cambiar password

	public String passwordactual="";
	
	//private static final String PREFRENCES_NAME = "sesiones_SharedPreferences";


    //----------------------------------------------------------------------------------------------
	public SQLiteDatabase conexOpen;// contiene la conexion a la base de datos
	private Context ccontexto;


	public void conectarDB(Context contexto) {
		BaseDeDatos objBaseDeDatos = new BaseDeDatos(ccontexto);
		objBaseDeDatos.openDataBase();
		this.conexOpen = objBaseDeDatos.myDataBase;// guardo la conexion a la base de datos

	}
	public void desConectarDB() {
		if (this.conexOpen != null){
			this.conexOpen.close();
		}
	}






			// CONSULTAR USUARIO Y CONTRASEÑA

	public int consultarUser(String user, String password,Context contexto) {
		int respuesta_db = 0;
		conectarDB(contexto);//concecto con la base de datos
		Cursor c = conexOpen.rawQuery("SELECT id, nombre, password FROM usuario where nombre='" +  user  + "'", null);
		if (c.moveToFirst()) {						//	Recorremos el cursor hasta que no haya mas registros.
			do {
				id_usuario = c.getInt(0);		//	id del usuario
				nombre = c.getString(1);		//  codigo del usuario, NO es el id
				password_db = c.getString(2);        //	codigo perfil-rol del usuario

			} while (c.moveToNext());
		}
		if ((nombre.equals(""))) {			//	El usuario no existe
			respuesta_db = 0;
		} else {
			if (password_db.equals(password)) {		//	Guardo en variables de la
				//  clase los datos del usuario
				respuesta_db = 1;
			} else {
				respuesta_db = 2;
			}
		}
		desConectarDB();//desconecto la base de datos
		return respuesta_db;
	}


	//LLENAR COMBO SEXO PROVEEDOR --> "SPINNER"
	public List<SpinnerObjectString> getSexo(Context contexto) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select  id, sexo from sexo", null);

		names.add(new SpinnerObjectString("0", "--Seleccione Sexo--"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getString(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

	//LLENAR COMBO ESTADO PROVEEDOR --> "SPINNER"
	public List<SpinnerObjectString> getEstado(Context contexto) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select  id, estado from estado", null);

		names.add(new SpinnerObjectString("0", "-Seleccione Estado-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getString(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}



	//LLENAR COMBO  PROVEEDOR  para producto--> "SPINNER"
	public List<SpinnerObjectString> getProveedor(Context contexto) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select  id, alias from proveedor where estado=1", null);

		names.add(new SpinnerObjectString("0", "-Seleccione Proveedor-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getString(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}

	//LLENAR COMBO  PRODUCTO  para entrdas--> "SPINNER"
	public List<SpinnerObjectString> getProducto(Context contexto, String id_proveedor) {

		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select  id, nombre_producto from productos where  estado=1 and  id_proveedor="+id_proveedor, null);

		names.add(new SpinnerObjectString("0", "-Seleccione Producto-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getString(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}




	// INSERTAR PROVEEDOR
	public void insertProveedor(Context contexto,String s_nombre, String s_apellido, String s_telefono, String s_direccion,String s_alias, String id_sexo,String id_estado){

		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("nombres_proveedor", s_nombre);
		newValues.put("apellidos_proveedor", s_apellido);
		newValues.put("telefono", s_telefono);
		newValues.put("direccion", s_direccion);
		newValues.put("alias", s_alias);
		newValues.put("id_sexo", id_sexo);
		newValues.put("estado", id_estado);

		conexOpen.insert("proveedor", null, newValues);

		desConectarDB();
	}

	// VARIBLES DEL PROVEEDOR ---> PARA MOSTRAR
	public void EditarProveedor(String id_proveedor,Context contexto){
		conectarDB(contexto);
		String query="select  t01.nombres_proveedor, t01.apellidos_proveedor,t01.alias,t01.direccion,t01.telefono,t02.sexo  from proveedor t01 inner join sexo t02 on  (t02.id=t01.id_sexo) where t01.id="+id_proveedor;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.nombres_proveedor		=cursor.getString(0);
				this.apellidos_proveedor	=cursor.getString(1);
				this.alias_proveedor		=cursor.getString(2);
				this.direccion_proveedor	=cursor.getString(3);
				this.telefono_proveedor		=cursor.getString(4);
				this.sexo_proveedor			=cursor.getString(5);
			} while (cursor.moveToNext());
		}
		desConectarDB();
	}

	// ID DEL SEXO DEL PROVEEDOR
	public void IDeSexo(String id_proveedor, Context contexto){
		conectarDB(contexto);
		String query="SELECT id_sexo   FROM  proveedor   where id="+id_proveedor;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.idsexo=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
	}


	// ID DEL SEXO DEL PROVEEDOR
	public void idestado(String id_proveedor, Context contexto){
		conectarDB(contexto);
		String query="SELECT estado   FROM  proveedor   where id="+id_proveedor;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.idestado=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
	}



	//LLENAR COMBO SEXO --> "SPINNER"
	public List<SpinnerObjectString> geteEditsexo(Context contexto) {
		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT id, sexo    FROM  sexo  order by sexo", null);

		names.add(new SpinnerObjectString(99, "--Seleccione Sexo--"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;

	}


	//LLENAR COMBO ESTADO --> "SPINNER"
	public List<SpinnerObjectString> geteEditestado(Context contexto) {
		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT id, estado    FROM  estado", null);

		names.add(new SpinnerObjectString(99, "-Seleccione Estado-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;

	}




	public ArrayList<RowReporteA> obtenerActividadesGenerales(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<RowReporteA> mFilas = new ArrayList<RowReporteA>();
		//Abrimos la conexion a la base de datos

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("SELECT  t01.id,(t01.nombres_proveedor  ||'  '||  t01.apellidos_proveedor)  nombrecompleto, \n" +
				"t02.sexo, t01.alias, t01.direccion,t01.telefono,t03.estado FROM proveedor  t01\n" +
				"inner join sexo t02 on (t02.id=t01.id_sexo) inner join estado t03 on (t03.id=t01.estado)", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			RowReporteA row = new RowReporteA();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.alias 		= cursor.getString(3);
					row.direccion 	= cursor.getString(4);
					row.telefono	= cursor.getString(5);
					row.estado	    = cursor.getString(6);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new RowReporteA();
					//actualizamos la fecha actual
					//currentFecha = cursor.getString(0);
					//colocamos los datos en el nuevo objeto
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.alias 		= cursor.getString(3);
					row.direccion 	= cursor.getString(4);
					row.telefono	= cursor.getString(5);
					row.estado	    = cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}

//contexto,id_proveedor, mnombres,mapellidos,mtelefono,mdireccion,malias,id_sexo
	//ACTUALIZAR PROVEEDOR
	public void actualizarProveedor(Context contexto, String id_proveedor, String mnombres, String mapellidos, String mtelefono, String mdireccion,String malias, int id_sexo, int id_estado  ){
		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("nombres_proveedor", mnombres);
		newValues.put("apellidos_proveedor", mapellidos);
		newValues.put("alias",malias);
		newValues.put("direccion", mdireccion);
		newValues.put("telefono", mtelefono);
		newValues.put("id_sexo", id_sexo);
		newValues.put("estado", id_estado);
		conexOpen.update("proveedor", newValues, "id="+id_proveedor, null);

		desConectarDB();
	}


	//ELIMINAR  PROVEEDOR
	public void eliminarProveedor (Context contexto, String id){

		conectarDB(contexto);
		conexOpen.delete("proveedor", "id=" + id, null);

		desConectarDB();
	}


		// buscar proveedor
		public ArrayList<RowReporteA> obtenerActividades_proveedor(Context contexto, String s_nombre, String s_apellido, String s_telefono, String s_direccion, String id_sexo, String s_alias, String id_estado){

			conectarDB(contexto);
			//Creamos una lista actividades generales
			ArrayList<RowReporteA> mFilas = new ArrayList<RowReporteA>();
			//Abrimos la conexion a la base de datos



			String query="";


			//	NOMBRES
			if(s_nombre.equals("")){
				//no pasa nada
			}else{
				query+="  t01.nombres_proveedor   LIKE '%"+s_nombre+"%'		and";
			}

			//	APELLIDOS
			if(s_apellido.equals("")){
				//no pasa nada
			}else{
				query+="  t01.apellidos_proveedor   LIKE '%"+s_apellido+"%'		and";
			}

			//	TELEFONO
			if(s_telefono.equals("")){
				//no pasa nada
			}else{
				query+="  t01.telefono   LIKE '%"+s_telefono+"%'		and";
			}

			//	DIRECCION
			if(s_direccion.equals("")){
				//no pasa nada
			}else{
				query+="  t01.direccion  LIKE '%"+s_direccion+"%'		and";
			}



			//	SEXO
			if(id_sexo.equals("0")){
				//no pasa nada
			}else{

				query+="  t01.id_sexo ="+"'"+id_sexo+"' and";
			}

			//	ESTADO
			if(id_estado.equals("0")){
				//no pasa nada
			}else{

				query+="  t01.estado ="+"'"+id_estado+"' and";
			}

				//	ALIAS
			if(s_alias.equals("")){
				//no pasa nada
			}else{
				query+=" t01.alias    LIKE '%"+s_alias+"%'		and";
			}


			query=query.substring(0, query.length()-4);

			//Realizamos la consulta a la base de datos
			Cursor cursor = conexOpen.rawQuery("SELECT  t01.id,(t01.nombres_proveedor  ||'  '||  t01.apellidos_proveedor)  nombrecompleto, \n" +
					"t02.sexo, t01.alias, t01.direccion,t01.telefono,t03.estado FROM proveedor  t01\n" +
					"inner join sexo t02 on (t02.id=t01.id_sexo) inner join estado t03 on (t03.id=t01.estado) where "+query, null);
			//Verificamos la existencia de datos en la consulta
			if(cursor.moveToFirst()){
				RowReporteA row = new RowReporteA();
				//Colocamos como fecha actual la prinera fecha
				String currentFecha=cursor.getString(0);
				while (cursor.isAfterLast()==false) {

					if(currentFecha.compareTo(cursor.getString(0))==0){
						row.id 			= cursor.getString(0);
						row.nombre 		= cursor.getString(1);
						row.sexo 		= cursor.getString(2);
						row.alias 		= cursor.getString(3);
						row.direccion 	= cursor.getString(4);
						row.telefono	= cursor.getString(5);
						row.estado	    = cursor.getString(6);
					}else{
						//lo agregamos al listado de filas del reporte A
						mFilas.add(row);
						//Creamos un nuevo objeto de fila
						row = new RowReporteA();
						//actualizamos la fecha actual
						//currentFecha = cursor.getString(0);
						//colocamos los datos en el nuevo objeto
						row.id 			= cursor.getString(0);
						row.nombre 		= cursor.getString(1);
						row.sexo 		= cursor.getString(2);
						row.alias 		= cursor.getString(3);
						row.direccion 	= cursor.getString(4);
						row.telefono	= cursor.getString(5);
						row.estado	    = cursor.getString(6);
					}
					//Nos movemos al siguiente registro
					cursor.moveToNext();
				}
				//Agregamos el ultimo objeto de fila creado
				mFilas.add(row);
			}
			//Cerramos la conexio de la base datos
			//closeDatabase();
			desConectarDB();
			return mFilas;
		}


	// FIN MANTENIMIENTO PROVEEDOR


				// MANTENIMIENTO PARA LOS PRODUCTOS


	// INSERTAR PROVEEDOR
	public void insertProducto(Context contexto,String s_nombre_producto, String s_codigo_producto, String id_proveedor,String id_estado){

		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("nombre_producto", s_nombre_producto);
		newValues.put("codigo_producto", s_codigo_producto);
		newValues.put("id_proveedor", id_proveedor);
		newValues.put("estado", id_estado);

		conexOpen.insert("productos", null, newValues);

		desConectarDB();
	}


	// CONSULTA PARA LA TABLA
	public ArrayList<ReporteProductos> consultaProductos(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteProductos> mFilas = new ArrayList<ReporteProductos>();
		//Abrimos la conexion a la base de datos

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("select  t01.id, t01.nombre_producto, t01.codigo_producto, t02.alias,t03.estado from productos t01 inner join proveedor t02 on (t02.id=t01.id_proveedor) \n" +
				"inner join estado t03 on (t03.id=t01.estado) ", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteProductos row = new ReporteProductos();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 				= cursor.getString(0);
					row.nombre_producto	= cursor.getString(1);
					row.codigo_producto = cursor.getString(2);
					row.proveedor 		= cursor.getString(3);
				    row.estado	    	= cursor.getString(4);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteProductos();
					//actualizamos la fecha actual
					//currentFecha = cursor.getString(0);
					//colocamos los datos en el nuevo objeto
					row.id 			= cursor.getString(0);
					row.id 				= cursor.getString(0);
					row.nombre_producto	= cursor.getString(1);
					row.codigo_producto = cursor.getString(2);
					row.proveedor 		= cursor.getString(3);
				 	row.estado	    	= cursor.getString(4);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	// CONSULTA PARA LA TABLA de busqueda
	public ArrayList<ReporteProductos> consultaProductosbusqueda(Context contexto, String s_nombre_producto, String s_codigo_producto, String id_proveedor,String id_estado){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteProductos> mFilas = new ArrayList<ReporteProductos>();
		//Abrimos la conexion a la base de datos

		String query="";

		//	producto
		if(s_nombre_producto.equals("")){
			//no pasa nada
		}else{
			query+="	t01.nombre_producto   LIKE '%"+s_nombre_producto+"%'  AND";

		}

		//	codigo
		if(s_codigo_producto.equals("")){
			//no pasa nada
		}else {
			query += "	t01.codigo_producto   LIKE '%" + s_codigo_producto + "%'		AND";

		}

		if(id_proveedor.equals("0")){
			//no pasa nada
		}else{
			query+="	t01.id_proveedor ="+"'"+id_proveedor+"' AND";

		}

		if(id_estado.equals("0")){
			//no pasa nada
		}else{
			query+="	t01.estado ="+"'"+id_estado+"' AND";

		}



	 	query=query.substring(0, query.length()-4);


		String consulta="select  t01.id, t01.nombre_producto, t01.codigo_producto, t02.alias,t03.estado from productos t01 inner join proveedor t02 on (t02.id=t01.id_proveedor) inner join estado t03 on (t03.id=t01.estado) WHERE 	";

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery(consulta+query, null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteProductos row = new ReporteProductos();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 				= cursor.getString(0);
					row.nombre_producto	= cursor.getString(1);
					row.codigo_producto = cursor.getString(2);
					row.proveedor 		= cursor.getString(3);
					row.estado	    	= cursor.getString(4);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteProductos();
					//actualizamos la fecha actual
					//currentFecha = cursor.getString(0);
					//colocamos los datos en el nuevo objeto
					row.id 			= cursor.getString(0);
					row.id 				= cursor.getString(0);
					row.nombre_producto	= cursor.getString(1);
					row.codigo_producto = cursor.getString(2);
					row.proveedor 		= cursor.getString(3);
					row.estado	    	= cursor.getString(4);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	//ELIMINAR  PRODUCTO
	public void eliminarProducto(Context contexto, String id){

		conectarDB(contexto);
		conexOpen.delete("productos", "id=" + id, null);

		desConectarDB();
	}

	//ELIMINAR  PRODUCTO
	public void eliminarEntrada(Context contexto, String id){

		conectarDB(contexto);
		conexOpen.delete("entradas", "id=" + id, null);

		desConectarDB();
	}


	// BARIABLES PARA ACTUALIZAR PRODUCTO
	public void EditarProducto(Context contexto,  String id_producto){
		conectarDB(contexto);
		String query="select nombre_producto, codigo_producto,id_proveedor,estado from productos where id= "+id_producto;
		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()){
			do{

				this.nombre_producto = cursor.getString(0);
				this.codigo_producto = cursor.getString(1);
				this.id_proveedor = cursor.getInt(2);
				this.id_estado=cursor.getInt(3);

			}while (cursor.moveToNext());

		}
		desConectarDB();
	}


	// BARIABLES PARA ACTUALIZAR PRODUCTO
	public void EditarEntradas(Context contexto,  String id_entrada){
		conectarDB(contexto);
		String query="SELECT   cantidad_producto, fecha_vencimiento, id_proveedor, id_producto FROM entradas  where id="+id_entrada;
		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()){
			do{

				this.cantidad_producto = cursor.getString(0);
				this.fecha_vencimiento = cursor.getString(1);
				this.id_proveedorentradas = cursor.getInt(2);
				this.id_productoentradas = cursor.getInt(3);

			}while (cursor.moveToNext());

		}
		desConectarDB();
	}





	//LLENAR COMBO proveedor de productos --> "SPINNER"
	public List<SpinnerObjectString> geteEditeproveedor(Context contexto) {
		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select id, alias from proveedor", null);

		names.add(new SpinnerObjectString(99, "-Seleccione Proveedor-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;

	}

	//LLENAR COMBO proveedor de productos --> "SPINNER"
	public List<SpinnerObjectString> geteEditeproducto(Context contexto,int  id_proveedor) {
		conectarDB(contexto);
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select id, nombre_producto from productos where id_proveedor="+id_proveedor, null);



		names.add(new SpinnerObjectString(99, "-Seleccione Producto-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;

	}


	// Actualizar producto

	public void actualizarproducto(Context contexto,String s_nombre_producto, String s_codigo_producto,int  id_proveedor,int id_estado, String id_producto){

		conectarDB(contexto);

		ContentValues newvalues = new ContentValues();
		newvalues.put("nombre_producto",s_nombre_producto);
		newvalues.put("codigo_producto",s_codigo_producto);
		newvalues.put("id_proveedor",id_proveedor);
		newvalues.put("estado",id_estado);
		conexOpen.update("productos", newvalues, "id=" + id_producto, null);
		desConectarDB();

	}


	///////////////// **** MANTENIMIENTO PARA CLIENTES


	public void ingresarcliente(Context contexto, String s_nombres_cliente, String s_apellidos_clientes,String s_telefono_cliente,String s_direccion_cliente,String s_codigo_cliente,String id_sexo, String id_estado){
	 conectarDB(contexto);

		ContentValues newvalues= new ContentValues();
		newvalues.put("nombres_cliente",s_nombres_cliente);
		newvalues.put("apellidos_cliente",s_apellidos_clientes);
		newvalues.put("telefono_cliente",s_telefono_cliente);
		newvalues.put("direccion_cliente",s_direccion_cliente);
		newvalues.put("codigo_cliente",s_codigo_cliente);
		newvalues.put("id_sexo",id_sexo);
		newvalues.put("estado",id_estado);

		conexOpen.insert("clientes", null, newvalues);


	}

	public ArrayList<Tablaclientes> generartablacliente(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<Tablaclientes> mFilas = new ArrayList<Tablaclientes>();
		//Abrimos la conexion a la base de datos

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("SELECT  t01.id,(t01.nombres_cliente  ||'  '||  t01.apellidos_cliente)  nombrecompleto, \n" +
				"t02.sexo, t01.codigo_cliente, t01.direccion_cliente,t01.telefono_cliente,t03.estado FROM clientes  t01 inner join sexo t02 on (t02.id=t01.id_sexo) inner join estado t03 on (t03.id=t01.estado)", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			Tablaclientes row = new Tablaclientes();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.codigo 		= cursor.getString(3);
					row.direccion 	= cursor.getString(4);
					row.telefono	= cursor.getString(5);
					row.estado	    = cursor.getString(6);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new Tablaclientes();
					//actualizamos la fecha actual
					//currentFecha = cursor.getString(0);
					//colocamos los datos en el nuevo objeto
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.codigo 		= cursor.getString(3);
					row.direccion 	= cursor.getString(4);
					row.telefono	= cursor.getString(5);
					row.estado	    = cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	//ELIMINAR  PRODUCTO
	public void eliminarCliente(Context contexto, String id){

		conectarDB(contexto);
		conexOpen.delete("clientes", "id=" + id, null);

		desConectarDB();
	}


	// VARIBLES DEL CLIENTE ---> PARA MOSTRAR
	public void EditarCliente(String id_cliente,Context contexto){
		conectarDB(contexto);
		String query="SELECT nombres_cliente, apellidos_cliente, direccion_cliente, telefono_cliente,codigo_cliente,estado,id_sexo FROM clientes where id="+id_cliente;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.nombres_cliente	=cursor.getString(0);
				this.apellidos_cliente	=cursor.getString(1);
				this.direccion_cliente	=cursor.getString(2);
				this.telefono_cliente	=cursor.getString(3);
				this.codigo_cliente		=cursor.getString(4);
				this.idestado_cliente	=cursor.getInt(5);
				this.idsexo_cliente		=cursor.getInt(6);

			} while (cursor.moveToNext());
		}
		desConectarDB();
	}





	public void modificarcliente(Context contexto, String  s_nombre, String s_apellidos, String s_telefono, String s_direccion, String s_codigo, int  id_sexo, int id_estado , String id_cliente){

		 conectarDB(contexto);

			ContentValues newvalues = new ContentValues();
			newvalues.put("nombres_cliente",s_nombre);
			newvalues.put("apellidos_cliente",s_apellidos);
			newvalues.put("telefono_cliente",s_telefono);
		    newvalues.put("direccion_cliente",s_direccion);
		    newvalues.put("codigo_cliente",s_codigo);
		    newvalues.put("id_sexo",id_sexo);
			newvalues.put("estado",id_estado);

		conexOpen.update("clientes",newvalues,"id="+id_cliente, null);

		 desConectarDB();

	}



	public ArrayList<Tablaclientes> generarbusquedatablacliente(Context contexto,  String  s_nombres_cliente, String s_apellidos_clientes,  String s_telefono_cliente, String s_direccion_cliente,  String s_codigo_cliente,  String id_sexo, String id_estado){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<Tablaclientes> mFilas = new ArrayList<Tablaclientes>();
		//Abrimos la conexion a la base de datos

		String query="";

		// NOMBRE
		if (s_nombres_cliente.equals("")){

		}else{
			query += "	t01.nombres_cliente  LIKE '%" + s_nombres_cliente + "%'		AND";
		}

		//APELLIDO
		if (s_apellidos_clientes.equals("")){
		}else {
			query += "	t01.apellidos_cliente  LIKE '%" + s_apellidos_clientes + "%'		AND";
		}


		// TELEFONO
		if (s_telefono_cliente.equals("")){
		} else {
			query += "	t01.telefono_cliente  LIKE '%" + s_telefono_cliente + "%'		AND";
		}


		// DIRECCION
		if (s_direccion_cliente.equals("")){
         }else {
			query += "	t01.direccion_cliente  LIKE '%" + s_direccion_cliente + "%'		AND";
		}



		// SEXO
		if (id_sexo.equals("0")){
          }else{
			query+="	t01.id_sexo ="+"'"+id_sexo+"' AND";
		}



		// ESTADO
		if (id_estado.equals("0")){


		}else {

			query+="	t01.estado ="+"'"+id_estado+"' AND";
		}


		// CÓDIGO
		if (s_codigo_cliente.equals("")){
          }else{
			query += "	t01.codigo_cliente  LIKE '%" + s_codigo_cliente + "%'		AND";
		}

		query=query.substring(0, query.length()-4);

		//Log.i("QUERY --->  ", query);


		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("SELECT  t01.id,(t01.nombres_cliente  ||'  '||  t01.apellidos_cliente)  nombrecompleto, \n" +
				"t02.sexo, t01.codigo_cliente, t01.direccion_cliente,t01.telefono_cliente,t03.estado FROM clientes  t01 inner join sexo t02 on (t02.id=t01.id_sexo) inner join estado t03 on (t03.id=t01.estado) where "+query, null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			Tablaclientes row = new Tablaclientes();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.codigo 		= cursor.getString(3);
					row.direccion 	= cursor.getString(4);
					row.telefono	= cursor.getString(5);
					row.estado	    = cursor.getString(6);
				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new Tablaclientes();
					//actualizamos la fecha actual
					//currentFecha = cursor.getString(0);
					//colocamos los datos en el nuevo objeto
					row.id 			= cursor.getString(0);
					row.nombre 		= cursor.getString(1);
					row.sexo 		= cursor.getString(2);
					row.codigo 		= cursor.getString(3);
					row.direccion 	= cursor.getString(4);
					row.telefono	= cursor.getString(5);
					row.estado	    = cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	// consultar la fecha actual
	public void consultarfecha(Context contexto){
		conectarDB(contexto);
		String query="SELECT date('now')  fecha;";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.fecha	=cursor.getString(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}

	// consultar el ultimo lote
	public void consultarultimolote(Context contexto){
		conectarDB(contexto);
		String query="SELECT MAX(lote) lote FROM entradas;";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.lote	=cursor.getInt(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}



	public void agregarentradas(Context contexto, String id_proveedor, String id_producto, String s_cantidad_producto, String s_fecha_vencimiento, String  fecha, int lote ){
		conectarDB(contexto);

		int estado= 2;

		ContentValues newValues= new ContentValues();
		newValues.put("id_proveedor",id_proveedor);
		newValues.put("id_producto", id_producto);
		newValues.put("cantidad_producto",s_cantidad_producto);
		newValues.put("fecha_vencimiento",s_fecha_vencimiento);
		newValues.put("fecha",fecha);
		newValues.put("lote",lote);
		newValues.put("estado_entrada",estado);

		conexOpen.insert("entradas", null, newValues);


		desConectarDB();
	}




	// CONSULTA PARA LA TABLA  TablaEntradas
	public ArrayList<TablaEntradas> consultaentradas(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<TablaEntradas> mFilas = new ArrayList<TablaEntradas>();
		//Abrimos la conexion a la base de datos

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("select t01.id,  \n" +
				"          t02.alias, \n" +
				"          t03.nombre_producto, \n" +
				"          t01.cantidad_producto, \n" +
				"          t01.fecha_vencimiento, \n" +
				"          t01.fecha, \n" +
				"          t01.lote,\n" +
				"          t04.estado \n" +
				"from entradas t01  \n" +
				"inner join proveedor t02 on (t02.id=t01.id_proveedor ) \n" +
				"inner join productos t03 on (t03.id=t01.id_producto)\n" +
				"inner join estado_entrada t04 on (t04.id=t01.estado_entrada) ", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			TablaEntradas row = new TablaEntradas();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 					= cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.producto 			= cursor.getString(2);
					row.cantidad_producto	= cursor.getString(3);
					row.fechavencimiento	= cursor.getString(4);
					//row.fecha				= cursor.getString(5);
					row.fecha				= cursor.getString(6);
					row.estado				= cursor.getString(7);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new TablaEntradas();


					row.id 					= cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.producto 			= cursor.getString(2);
					row.cantidad_producto	= cursor.getString(3);
					row.fechavencimiento	= cursor.getString(4);
					//row.fecha				= cursor.getString(5);
					row.fecha				= cursor.getString(6);
					row.estado				= cursor.getString(7);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	// CONSULTA PARA LA TABLA  TablaEntradas
	public ArrayList<TablaEntradas> consultaentradasbusqueda(Context contexto,String id_proveedor,  String id_producto, String s_cantidad_producto,  String s_fecha_vencimiento){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<TablaEntradas> mFilas = new ArrayList<TablaEntradas>();
		//Abrimos la conexion a la base de datos


		String query="";

		//	producto
		if(id_producto.equals("0")){
			//no pasa nada
		}else{
			query+="	t03.id  = "+id_producto+"   AND";

		}

		//	proveedor
		if(id_proveedor.equals("0")){
			//no pasa nada
		}else {
			query += "	t02.id  ="+id_proveedor+ "	AND";

		}

		//	cantidad
		if(s_cantidad_producto.equals("")){
			//no pasa nada
		}else {
			query += "	t01.cantidad_producto ="+s_cantidad_producto+ "	AND";

		}



		if(s_fecha_vencimiento.equals("")){
			//no pasa nada
		}else{
			query+="	t01.fecha_vencimiento   LIKE '%"+s_fecha_vencimiento+"%'  AND";

		}



		query=query.substring(0, query.length()-4);





		//Realizamos la consulta a la base de datos
		String consulta="select t01.id,  \n" +
				"          t02.alias, \n" +
				"          t03.nombre_producto, \n" +
				"          t01.cantidad_producto, \n" +
				"          t01.fecha_vencimiento, \n" +
				"          t01.fecha, \n" +
				"          t01.lote,\n" +
				"          t04.estado \n" +
				"from entradas t01  \n" +
				"inner join proveedor t02 on (t02.id=t01.id_proveedor ) \n" +
				"inner join productos t03 on (t03.id=t01.id_producto)\n" +
				"inner join estado_entrada t04 on (t04.id=t01.estado_entrada) where  ";


		Log.i("busqueda entrada",consulta+query);

		Cursor cursor = conexOpen.rawQuery(consulta+query, null);

		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			TablaEntradas row = new TablaEntradas();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 					= cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.producto 			= cursor.getString(2);
					row.cantidad_producto	= cursor.getString(3);
					row.fechavencimiento	= cursor.getString(4);
					//row.fecha				= cursor.getString(5);
					row.fecha				= cursor.getString(6);
					row.estado				= cursor.getString(7);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new TablaEntradas();


					row.id 					= cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.producto 			= cursor.getString(2);
					row.cantidad_producto	= cursor.getString(3);
					row.fechavencimiento	= cursor.getString(4);
					//row.fecha				= cursor.getString(5);
					row.fecha				= cursor.getString(6);
					row.estado				= cursor.getString(7);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}








	// VARIBLES DETALLE ---> PARA MOSTRAR
	public void detalleProveedor(Context contexto, String id_proveedor){
		conectarDB(contexto);
		String query="SELECT  alias  from proveedor  where id="+id_proveedor;
		Log.i("QUERY --->  ", query);
		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.proveedor_detalle	=cursor.getString(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}

	// VARIBLES DETALLE ---> PARA MOSTRAR
	public void detalleProducto(Context contexto, String id_producto){
		conectarDB(contexto);
		String query="SELECT  nombre_producto  from productos where id="+id_producto;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.producto_detalle	=cursor.getString(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}


	// VARIBLES DETALLE ---> PARA MOSTRAR
	public void detalleCliente(Context contexto, String id_cliente){
		conectarDB(contexto);
		String query="select  (t01.nombres_cliente  ||'  '||  t01.apellidos_cliente)  nombrecompleto from clientes t01 where id ="+id_cliente;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.cliente_detalle	=cursor.getString(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}

	//proveedor_detalle,producto_detalle, cliente_detalle;


	// LLENAR LISTA -->  TODOS LOS clientes
	//BUSCAR PACIENTE
	public ArrayList<SpinnerObject> listaPacientes(Context contexto, String filtro) {

		conectarDB(contexto);
		String cadena ="where estado=1  and   (nombres_cliente   ||' '|| apellidos_cliente  )";

		String cadena_filtro="";

		if(filtro.equals(""))
		{
			cadena_filtro="where estado=1 ";
		}
		else
		{
			cadena_filtro=cadena+filtro;
		}

		ArrayList<SpinnerObject> names = new ArrayList<SpinnerObject>();
		Cursor cursor = conexOpen
				.rawQuery("SELECT id,  ( nombres_cliente   ||' '|| apellidos_cliente   ) AS nombre   FROM  clientes    " + cadena_filtro + "order by nombres_cliente asc", null);
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObject(cursor.getInt(0),  cursor.getString(1) ));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;
	}



	// VARIBLES DETALLE ---> PARA MOSTRAR
	public void cliente(Context contexto, int  id_cliente){
		conectarDB(contexto);
		String query="select  (t01.nombres_cliente  ||'  '||  t01.apellidos_cliente)  nombrecompleto from clientes t01 where estado=1 and id="+id_cliente;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.nombre_cliente	=cursor.getString(0);


			} while (cursor.moveToNext());
		}
		desConectarDB();
	}


	//LLENAR COMBO  LOTE  para DETALLE--> "SPINNER"
	public List<SpinnerObjectString> getLote(Context contexto, String id_producto) {

		conectarDB(contexto);
		String complemento="	group by t01.id, t02.lote";
		//select lote, (lote  ||' --  '|| cantidad_producto)  from entradas  where id_producto
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT   lote, ( lote  ||' -- '||  (  entrada- SUM(suma))) as total, vencimiento \n" +
				"  FROM\n" +
				"(    \n" +
				"         \n" +
				"         SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote,  t02.fecha_vencimiento vencimiento, \n" +
				"         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"         left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"         left join  temporal_detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"         where t04.id=2 and t01.id="+id_producto + "  \n" +
				"         group by t01.id, t02.lote  \n" +
				"\n" +
				"\n" +
				"       union all \n" +
				"\n" +
				"\n" +
				"       SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote,  t02.fecha_vencimiento vencimiento, \n" +
				"         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"         left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"         left join  detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"         where t04.id=2 and t01.id="+id_producto+"  \n" +
				"         group by t01.id, t02.lote  \n" +
				"     )  group by lote order by  vencimiento  asc", null);



		names.add(new SpinnerObjectString("0", "-Seleccione Lote-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getString(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;



	}

					//SEGUNDA VEZ
	//LLENAR COMBO  LOTE  para DETALLE--> "SPINNER"
	public List<SpinnerObjectString> getLotesegundaves(Context contexto, String id_producto) {

		conectarDB(contexto);
		String complemento="	group by t01.id, t02.lote";
		//select lote, (lote  ||' --  '|| cantidad_producto)  from entradas  where id_producto
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("select  t01.nombre_producto, \n" +
				"          t02.cantidad_producto, \n" +
				"          t02.lote, \n" +
				"         case when ( sum(t04.cantidad))is null then 0\n" +
				"         else sum(t04.cantidad) end sumatemporal\n"+

				"from productos  t01  \n" +
				"left join entradas t02 on (t02 .id_producto=t01.id)\n" +
				"left join temporal_detalle_salida  t04 on (t04.lote=t02.lote)" +
				"inner join estado_entrada t05 on (t05.id=t02.estado_entrada)  where t05.id=2 and t01.id="+id_producto+complemento, null);

		names.add(new SpinnerObjectString("0", "-Seleccione Lote-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getString(2), cursor.getString(3)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;



	}

	public int  vercantidadsegunda(Context contexto,String id_producto){
		conectarDB(contexto);

		int cantidadexistecia=0;
		String complemento="	group by t01.id, t02.lote";
		String query=" select  t01.nombre_producto, \n" +
				"          t02.cantidad_producto, \n" +
				"          t02.lote, \n" +



				"         case when ( sum(t04.cantidad))is null then 0\n" +
				"         else sum(t04.cantidad) end sumatemporal\n" +

				"from productos  t01  \n" +
				"left join entradas t02 on (t02 .id_producto=t01.id)\n" +

				"left join temporal_detalle_salida  t04 on (t04.lote=t02.lote)  where t01.id=" + id_producto+ complemento;
Log.i("mostrar---->", query);
		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidadexistecia		=cursor.getInt(3);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidadexistecia;
	}


	public int  vercantidad(Context contexto,String id_producto,String id_lote){
		conectarDB(contexto);

		int cantidadexistecia=0;
		String complemento="	group by t01.id, t02.lote";
		String query="SELECT   lote,   (  entrada- SUM(suma)) as total \n" +
				"  FROM\n" +
				"(    \n" +
				"         \n" +
				"         SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote, \n" +
				"         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"         left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"         left join  temporal_detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"         where t04.id=2 and t01.id="+id_producto+"    and t02.lote="+id_lote+"\n" +
				"         group by t01.id, t02.lote  \n" +
				"\n" +
				"\n" +
				"       union \n" +
				"\n" +
				"\n" +
				"       SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote, \n" +
				"         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"         left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"         left join  detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"         where t04.id=2 and t01.id="+id_producto+"   and t02.lote="+id_lote+"\n" +
				"         group by t01.id, t02.lote  \n" +
				"     )  group by lote " ;
//Log.i("mostrar---->",query);
		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidadexistecia		=cursor.getInt(1);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidadexistecia;
	}


	public int  vercantidadPrimeravez(Context contexto,String id_producto,String id_lote){
		conectarDB(contexto);

		int cantidadexistecia=0;
		String complemento="	group by t01.id, t02.lote";
		String query=" select  t01.nombre_producto, \n" +
				"          t02.cantidad_producto, \n" +
				"          t02.lote, \n" +
				"          t03.lote, \n" +
				"          case when (sum(t03.cantidad)) is null  then 0 \n" +
				"          else sum(t03.cantidad) end sumasalida,\n" +
				"         case when ( sum(t04.cantidad))is null then 0\n" +
				"         else sum(t04.cantidad) end sumatemporal,\n" +
				"        \n" +
				"\n" +
				"    CASE\n" +
				"        WHEN ( t02.cantidad_producto - sum(t04.cantidad)   ) is null THEN      t02.cantidad_producto\n" +
				"       else\n" +
				"     ( t02.cantidad_producto - sum(t04.cantidad)    )  end existencia   \n" +
				"\n" +
				" from productos  t01  \n" +
				"left join entradas t02 on (t02 .id_producto=t01.id)\n" +
				"left join detalle_salida t03 on (t03.id_producto=t01.id)  \n" +
				"left join temporal_detalle_salida  t04 on (t04.id_producto=t01.id) where t02.id=" + id_producto+  " and t02.lote="+id_lote+ complemento;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidadexistecia		=cursor.getInt(6);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidadexistecia;
	}


	public int  vercantidadsalidaguardar(Context contexto,String id_producto,String id_lote){
		conectarDB(contexto);

		int cantidad=0;

		String query="SELECT count (*) cantidad  FROM detalle_salida   where id_producto="+ id_producto + " and lote="+id_lote;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidad		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidad;
	}

	public int  vercantidadsalida(Context contexto,String id_producto){
		conectarDB(contexto);

		int cantidad=0;

		String query="SELECT count (*) cantidad  FROM detalle_salida   where id_producto="+ id_producto;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidad		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidad;
	}



	public ArrayList<ReporteDetalles> generartabladetalle(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteDetalles> mFilas = new ArrayList<ReporteDetalles>();
		//Abrimos la conexion a la base de datos

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("select t01.id ,\n" +
				"( t03.nombres_cliente ||'  '||  t03.apellidos_cliente)nombre ,  t04.nombre_producto ,t01.cantidad , t01.lote ,t05.fecha_vencimiento \n" +
				"from temporal_detalle_salida t01 \n" +
				"                                inner join  salidas t02 on (t02.id=t01.id_salida)\n" +
				"                                inner join clientes t03 on (t03.id=t02.id_cliente)\n" +
				"                                inner join productos t04 on (t04.id=t01.id_producto)\n" +
				"                                inner join entradas t05 on (t05.lote=t01.lote)", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteDetalles row = new ReporteDetalles();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 					= cursor.getString(0);
					row.cliente				= cursor.getString(1);
					row.nombre_producto		= cursor.getString(2);
					row.cantidad 			= cursor.getString(3);
					row.lote 				= cursor.getString(4);
					row.fechavencimiento	= cursor.getString(5);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteDetalles();

					//colocamos los datos en el nuevo objeto
					row.id 					= cursor.getString(0);
					row.cliente				= cursor.getString(1);
					row.nombre_producto		= cursor.getString(2);
					row.cantidad 			= cursor.getString(3);
					row.lote 				= cursor.getString(4);
					row.fechavencimiento	= cursor.getString(5);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}

	//ELIMINAR  DETALLE
	public void eliminardetalle(Context contexto, String id){

		conectarDB(contexto);
		conexOpen.delete("temporal_detalle_salida", "id=" + id, null);

		desConectarDB();
	}





	//ELIMINAR  detalle temporal
	public void eliminartodo_detalle (Context contexto){

		conectarDB(contexto);
		conexOpen.delete("temporal_detalle_salida", "" , null);

		desConectarDB();
	}




	public ArrayList<ReporteSalidas> generartablasalidas(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteSalidas> mFilas = new ArrayList<ReporteSalidas>();
		//Abrimos la conexion a la base de datos

		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("select t01.id ,\n" +
				"( t03.nombres_cliente ||'  '||  t03.apellidos_cliente)nombre , t06.alias,  t04.nombre_producto ,t01.cantidad , t01.lote ,t05.fecha_vencimiento \n" +
				"from  detalle_salida t01 \n" +
				"                                inner join  salidas t02 on (t02.id=t01.id_salida)\n" +
				"                                inner join clientes t03 on (t03.id=t02.id_cliente)\n" +
				"                                inner join productos t04 on (t04.id=t01.id_producto)\n" +
				"                                inner join entradas t05 on (t05.lote=t01.lote) " +
				"                                inner join proveedor t06 on (t06.id=t04.id_proveedor)", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteSalidas row = new ReporteSalidas();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 					= cursor.getString(0);
					row.cliente				= cursor.getString(1);
					row.proveedor		    = cursor.getString(2);
					row.nombre_producto		= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.lote 				= cursor.getString(5);
					row.fechavencimiento	= cursor.getString(6);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteSalidas();

					//colocamos los datos en el nuevo objeto
					row.id 					= cursor.getString(0);
					row.cliente				= cursor.getString(1);
					row.proveedor		    = cursor.getString(2);
					row.nombre_producto		= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.lote 				= cursor.getString(5);
					row.fechavencimiento	= cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}


	public void insertarSalida(Context contexto, String id_cliente){

		conectarDB(contexto);
		ContentValues newvalues = new ContentValues();
		newvalues.put("id_cliente",id_cliente);
		conexOpen.insert("salidas", null, newvalues);
		desConectarDB();

	}



	public int  consultarid_salida(Context contexto){
		conectarDB(contexto);

		int id_salida=0;

		String query=" SELECT id FROM salidas ORDER BY oid DESC  LIMIT 1";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				id_salida		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  id_salida;
	}


	public void insertardetalletemporal(Context contexto, int id_salida, String id_lote, String s_cantidad, String id_producto){

		conectarDB(contexto);

		ContentValues newvalues= new ContentValues();
		newvalues.put("id_salida",id_salida);
		newvalues.put("lote",id_lote);
		newvalues.put("cantidad",s_cantidad);
		newvalues.put("id_producto",id_producto);


		conexOpen.insert("temporal_detalle_salida", null, newvalues);

	}


	public int  consultarid_salidatemporal(Context contexto){
		conectarDB(contexto);

		int salida=0;

		String query="select count (*)  cantidad from temporal_detalle_salida";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				salida		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  salida;
	}


	//ELIMINAR Salida
	public void eliminarsalida (Context contexto, int  id_salida){

		conectarDB(contexto);
		conexOpen.delete("salidas", "id=" + id_salida, null);

		desConectarDB();
	}


	// INSERTAR PROVEEDOR
	public void insertartodoslosdetalles(Context contexto   ){

		conectarDB(contexto);
		String query="select    id_salida, lote, cantidad, id_producto   from temporal_detalle_salida  ";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				ContentValues newValues = new ContentValues();

				newValues.put("id_salida", cursor.getString(0));
				newValues.put("lote", cursor.getString(1));
				newValues.put("cantidad", cursor.getString(2));
				newValues.put("id_producto", cursor.getString(3));

				conexOpen.insert("detalle_salida", null, newValues);


			} while (cursor.moveToNext());
		}



		desConectarDB();
	}


	public int  consultarregistrostemporales(Context contexto, int id_salida){
		conectarDB(contexto);

		int cantidad_salida=0;

		String query="select count (*) from  temporal_detalle_salida where id_salida="+id_salida;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidad_salida		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidad_salida;
	}

	public int  contarrregistrostemporales(Context contexto){
		conectarDB(contexto);

		int cantidad_salida=0;

		String query="select count (*) from  temporal_detalle_salida ";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidad_salida		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidad_salida;
	}



	public int  consultarcantidaddeentradasacambiarestado(Context contexto, String  id_producto){
		conectarDB(contexto);

		int cantidad_entrada=0;

		String query="SELECT count (*) from entradas \n" +
				"   where      lote=(select t01.lote  from   temporal_detalle_salida t01  \n" +
				" inner join   entradas t02 on (t02.lote=t01.lote )   where  estado_entrada=2)   and cantidad_producto=(\n" +
				"     SELECT     (   SUM(suma)) as total\n" +
				"                           FROM\n" +
				"\t\t\t\t  (   \n" +
				"\t\t\t\t         SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote,  \n" +
				"                                         t02.fecha_vencimiento vencimiento, \n" +
				"\t\t\t\t         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"\t\t\t\t         left join  entradas                         t02 on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t         left join  temporal_detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"\t\t\t\t         left join estado_entrada               t04 on (t04.id=t02.estado_entrada) \n" +
				"\t\t\t\t         where t04.id=2 and t01.id="+id_producto+"\n" +
				"\t\t\t\t         group by t01.id, t02.lote \n" +
				"\t\t\t\t\n" +
				"                        union all \n" +
				"\t\t\t\t\n" +
				"                                    SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote,  \n" +
				"                                        t02.fecha_vencimiento vencimiento, \n" +
				"\t\t\t\t        case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t        else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"\t\t\t\t             left join  entradas           t02  on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t             left join  detalle_salida   t03  on (t03.lote=t02.lote)\n" +
				"\t\t\t\t            left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"\t\t\t\t         where t04.id=2 and t01.id="+id_producto+"\n" +
				"\t\t\t\t         group by t01.id, t02.lote  \n" +
				"\t\t\t\t     )  group by lote order by  vencimiento  asc\n" +
				"\t\t\t\t     ) ";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidad_entrada		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		Log.i("count-->", query);
		desConectarDB();

		return  cantidad_entrada;

	}



	public int  consultarentradaid(Context contexto, String  id_producto){
		conectarDB(contexto);

		int id_entrada=0;

		String query="SELECT id from entradas \n" +
				"   where  lote=(select t01.lote  from   temporal_detalle_salida t01  \n" +
				" inner join   entradas t02 on (t02.lote=t01.lote )   where  estado_entrada=2)   and cantidad_producto=(\n" +
				"     SELECT     (   SUM(suma)) as total\n" +
				"                           FROM\n" +
				"\t\t\t\t  (   \n" +
				"\t\t\t\t         SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote,  \n" +
				"                        t02.fecha_vencimiento vencimiento, \n" +
				"\t\t\t\t         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"\t\t\t\t         		left join  entradas                    t02 on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t         		left join  temporal_detalle_salida     t03 on (t03.lote=t02.lote)\n" +
				"\t\t\t\t         		left join estado_entrada               t04 on (t04.id=t02.estado_entrada) \n" +
				"\t\t\t\t         where t04.id=2 and t01.id="+id_producto+"\n" +
				"\t\t\t\t         group by t01.id, t02.lote \n" +
				"\t\t\t\t\n" +
				"                        union  all\n" +
				"\t\t\t\t\n" +
				"               SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote,  \n" +
				"                      t02.fecha_vencimiento vencimiento, \n" +
				"\t\t\t\t        case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t        else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"\t\t\t\t             left join  entradas       t02 on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t             left join  detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"\t\t\t\t             left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"\t\t\t\t         where t04.id=2 and t01.id="+id_producto+"\n" +
				"\t\t\t\t         group by t01.id, t02.lote  \n" +
				"\t\t\t\t     )  group by lote order by  vencimiento  asc\n" +
				"\t\t\t\t     ) ";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				id_entrada		=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		Log.i("consultarentradaid -->", query);
		desConectarDB();

		return  id_entrada;

	}


	//ACTUALIZAR ESTADO DE LA ENTRADA
	public void actualizarestadoentradas(Context contexto,  int id_entrada  ){
		conectarDB(contexto);
		int estado=1;

		ContentValues newValues = new ContentValues();

		newValues.put("estado_entrada", estado);
		conexOpen.update("entradas", newValues, "id=" + id_entrada, null);

		desConectarDB();
	}






	public void detallemodificar(String id_detalle, Context contexto){
		conectarDB(contexto);
		String query="SELECT lote, cantidad FROM temporal_detalle_salida  where id="+id_detalle;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.id_lotemodificar=cursor.getInt(0);
				this.cantidad=cursor.getString(1);
				//this.lote=cursor.getInt()

			} while (cursor.moveToNext());
		}
		desConectarDB();
	}





	//LLENAR COMBO  LOTE  para DETALLE--> "SPINNER"
	public List<SpinnerObjectString> getLotemodificar(Context contexto, String id_producto ,int  n_id_lote) {

		conectarDB(contexto);
		String complemento="	group by t01.id, t02.lote";
		//select lote, (lote  ||' --  '|| cantidad_producto)  from entradas  where id_producto
		List<SpinnerObjectString> names = new ArrayList<SpinnerObjectString>();
		Cursor cursor = conexOpen.rawQuery("SELECT  lote,   ( lote  ||' --  '||  (   entrada- SUM(suma) ) ) as total \n" +
				"                   FROM\n" +
				"\t\t\t\t(    \n" +
				"\t\t\t\t        SELECT t01.nombre_producto, t02.cantidad_producto entrada , t02.lote lote, \n" +
				"\t\t\t\t         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t           else sum(t03.cantidad) end suma   \n" +
				"                                        FROM productos  t01\n" +
				"\t\t\t\t        left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t         left join  detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"\t\t\t\t         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"\t\t\t\t         where t04.id=2    and t01.id="+id_producto+"  and t02.lote="+n_id_lote+"  \n" +
				"\t\t\t\t         group by t01.id, t02.lote  \n" +
				"\t\t\t\t     )  group by lote  ", null);

		names.add(new SpinnerObjectString("99", "-Seleccione Lote-"));
		if (cursor.moveToFirst()) {
			do {
				names.add(new SpinnerObjectString(cursor.getInt(0), cursor.getString(1)));
			} while (cursor.moveToNext());
		}
		desConectarDB();
		return names;



	}




	public int  vercantidaddetalle(Context contexto,String id_producto,int id_lote){
		conectarDB(contexto);

		int cantidadexistecia=0;
		String complemento="	group by t01.id, t02.lote";
		String query=" select  t01.nombre_producto, \n" +
				"          t02.cantidad_producto, \n" +
				"          t02.lote, \n" +
				"          t03.lote, \n" +
				"          case when (sum(t03.cantidad)) is null  then 0 \n" +
				"          else sum(t03.cantidad) end sumasalida,\n" +
				"         case when ( sum(t04.cantidad))is null then 0\n" +
				"         else sum(t04.cantidad) end sumatemporal,\n" +
				"     (  t02.cantidad_producto -   case when (sum(t03.cantidad)) is null  then 0 \n" +
				"          else sum(t03.cantidad) end )   as existencia\n" +
				"from productos  t01  \n" +
				"left join entradas t02 on (t02 .id_producto=t01.id)\n" +
				"left join detalle_salida t03 on (t03.lote=t02.lote)  \n" +
				"left join temporal_detalle_salida  t04 on (t04.lote=t02.lote)  where t01.id=" + id_producto+  " and t02.lote="+id_lote+ complemento;
//Log.i("mostrar---->",query);
		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				cantidadexistecia		=cursor.getInt(6);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return  cantidadexistecia;
	}



	public int cantidad(String id_detalle, Context contexto){
		conectarDB(contexto);

		int cantidad=0;

		String query="SELECT  cantidad FROM temporal_detalle_salida  where id="+id_detalle;

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {


				cantidad=cursor.getInt(0);

			} while (cursor.moveToNext());
		}
		desConectarDB();
		return cantidad;
	}



	public void actualizarDetalle(Context contexto,int  cantidad, String id_detalle){

		conectarDB(contexto);

		ContentValues newvalues = new ContentValues();
		newvalues.put("cantidad",cantidad);

		conexOpen.update("temporal_detalle_salida", newvalues, "id=" + id_detalle, null);
		desConectarDB();

	}


	public ArrayList<ReporteExistencias> generartablasExistencias(Context contexto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteExistencias> mFilas = new ArrayList<ReporteExistencias>();
		//Abrimos la conexion a la base de datos





		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("SELECT id_entrada, proveedor, producto,   lote,    (  entrada- SUM(suma)) as total, vencimiento\n" +
				"\n" +
				"\t\t\t\t  FROM\n" +
				"\t\t\t\t(    \n" +
				"\t\t\t\t        \n" +
				"\t\t\t\t   SELECT t02.id id_entrada, t01.nombre_producto producto, \n" +
				"                                                t02.cantidad_producto entrada , \n" +
				"                                                t02.lote lote,  \n" +
				"                                                t02.fecha_vencimiento vencimiento,\n" +
				"                                                t05.alias proveedor,\n" +
				"\t\t\t\t         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"\t\t\t\t         left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t         left join  detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"\t\t\t\t         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"                                         left join proveedor t05 on (t05.id=t01.id_proveedor)\n" +
				"\t\t\t\t         where t04.id=2\n" +
				"\t\t\t\t         group by t01.id, t02.lote  \n" +
				"\t\t\t\t     )  group by lote order by  vencimiento  asc", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteExistencias row = new ReporteExistencias();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id			        = cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.nombre_producto		= cursor.getString(2);
					row.lote 				= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
                    row.fechavencimiento	= cursor.getString(5);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteExistencias();

					//colocamos los datos en el nuevo objeto
					row.id			        = cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.nombre_producto		= cursor.getString(2);
					row.lote 				= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.fechavencimiento	= cursor.getString(5);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}




	public ArrayList<ReporteExistencias> generartablasExistenciasbusqueda(Context contexto, String id_proveedor , String id_producto){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteExistencias> mFilas = new ArrayList<ReporteExistencias>();
		//Abrimos la conexion a la base de datos


		String query="";





		if(id_proveedor.equals("0")){
			//no pasa nada
		}else{
			query+="	and  t05.id="+"'"+id_proveedor+"'";

		}

		if(id_producto.equals("0")){
			//no pasa nada
		}else{
			query+="	and   t01.id="+"'"+id_producto+"' ";

		}



		//   query=query.substring(0, query.length()-4);



		String variable= "SELECT  proveedor, producto,   lote,    (  entrada- SUM(suma)) as total, vencimiento   FROM    SELECT t01.nombre_producto producto,   t02.cantidad_producto entrada , t02.lote lote,   t02.fecha_vencimiento vencimiento, t05.alias proveedor,  case when (sum(t03.cantidad)) is null  then 0  else sum(t03.cantidad) end suma   FROM productos  t01   left join  entradas t02 on (t02.id_producto=t01.id)         left join  detalle_salida t03 on (t03.lote=t02.lote)          left join estado_entrada  t04 on (t04.id=t02.estado_entrada)  left join proveedor t05 on (t05.id=t01.id_proveedor)         where t04.id=2 "+query+"        group by t01.id, t02.lote      )  group by lote order by  vencimiento  asc";

		// Log.i("exitecias -->",variable);
		//---	openDataBase();
		//Realizamos la consulta a la base de datos
		Cursor cursor = conexOpen.rawQuery("SELECT id_entrada, proveedor, producto,   lote,    (  entrada- SUM(suma)) as total, vencimiento\n" +
				"\n" +
				"\t\t\t\t  FROM\n" +
				"\t\t\t\t(    \n" +
				"\t\t\t\t        \n" +
				"\t\t\t\t   SELECT t02.id id_entrada, t01.nombre_producto producto, \n" +
				"                                                t02.cantidad_producto entrada , \n" +
				"                                                t02.lote lote,  \n" +
				"                                                t02.fecha_vencimiento vencimiento,\n" +
				"                                                t05.alias proveedor,\n" +
				"\t\t\t\t         case when (sum(t03.cantidad)) is null  then 0 \n" +
				"\t\t\t\t         else sum(t03.cantidad) end suma   FROM productos  t01 \n" +
				"\t\t\t\t         left join  entradas t02 on (t02.id_producto=t01.id)\n" +
				"\t\t\t\t         left join  detalle_salida t03 on (t03.lote=t02.lote)\n" +
				"\t\t\t\t         left join estado_entrada  t04 on (t04.id=t02.estado_entrada) \n" +
				"                                         left join proveedor t05 on (t05.id=t01.id_proveedor)\n" +
				"\t\t\t\t         where t04.id=2 "+query	 +  	"\n" +
				"\t\t\t\t         group by t01.id, t02.lote  \n" +
				"\t\t\t\t     )  group by lote order by  vencimiento  asc", null);
		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteExistencias row = new ReporteExistencias();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id			        = cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.nombre_producto		= cursor.getString(2);
					row.lote 				= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.fechavencimiento	= cursor.getString(5);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteExistencias();

					//colocamos los datos en el nuevo objeto
					row.id			        = cursor.getString(0);
					row.proveedor			= cursor.getString(1);
					row.nombre_producto		= cursor.getString(2);
					row.lote 				= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.fechavencimiento	= cursor.getString(5);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}



	// ver pas actual
	public int  passActual(Context contexto, String nombreusuario, String s_passActual){
		conectarDB(contexto);

		int respuesta=0;
		String query="SELECT  password  FROM usuario  where nombre="+"'"+nombreusuario+"'";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				this.passwordactual		=cursor.getString(0);

			} while (cursor.moveToNext());
		}


		if (passwordactual.equals(s_passActual)){

			respuesta=1;

		}
		desConectarDB();
		//Log.i("-->password", passwordactual +"pasdigitada"+ s_passActual+"respue"+respuesta);
		return  respuesta;
	}


	public void actualizarPassword(Context contexto,String  s_confirma_password ,  int  id_usuario ){
		conectarDB(contexto);

		ContentValues newValues = new ContentValues();
		newValues.put("password", s_confirma_password);

		conexOpen.update("usuario", newValues, "id=" + id_usuario, null);

		desConectarDB();
	}

	// ver pas actual
	public int  idusario(Context contexto, String nombreusuario ){
		conectarDB(contexto);

		int id_usuariopass=0;
		String query="SELECT  id  FROM usuario  where nombre="+"'"+nombreusuario+"'";

		Cursor cursor = conexOpen.rawQuery(query, null);
		if (cursor.moveToFirst()) {
			do {

				id_usuariopass	=cursor.getInt(0);

			} while (cursor.moveToNext());
		}

		desConectarDB();
		//Log.i("-->password", query );
		return  id_usuariopass;
	}


	// Actualizar producto

	public void actualizarEntrada(Context contexto,String s_fecha_vencimiento, String s_cantidad,int  id_proveedor, int id_producto , String id_entrada){

		conectarDB(contexto);

		ContentValues newvalues = new ContentValues();
		newvalues.put("fecha_vencimiento",s_fecha_vencimiento);
		newvalues.put("cantidad_producto",s_cantidad);
		newvalues.put("id_proveedor",id_proveedor);
		newvalues.put("id_producto",id_producto);

		conexOpen.update("entradas", newvalues, "id=" + id_entrada, null);
		desConectarDB();

	}


	public ArrayList<ReporteSalidas> generartablasalidasbusqueda(Context contexto, String id_proveedor , String id_producto , int  id_cliente){

		conectarDB(contexto);
		//Creamos una lista actividades generales
		ArrayList<ReporteSalidas> mFilas = new ArrayList<ReporteSalidas>();
		//Abrimos la conexion a la base de datos


		String query="";

		//	idproveedor
		if(id_proveedor.equals("0")){
			//no pasa nada
		}else{
			query+="	t06.id ="+"'"+id_proveedor+"' AND";

		}



		//	idproducto
		if(id_producto.equals("0")){
			//no pasa nada
		}else{
			query+="	t04.id ="+"'"+id_producto+"' AND";

		}

		//	idcliente
		if(id_cliente==0){
			//no pasa nada
		}else{
			query+="	t03.id ="+"'"+id_cliente+"' AND";

		}



		query=query.substring(0, query.length()-4);


		//---	openDataBase();
		//Realizamos la consulta a la base de datos

		String consulta="select t01.id ,\n" +
				"( t03.nombres_cliente ||'  '||  t03.apellidos_cliente)nombre , t06.alias,  t04.nombre_producto ,t01.cantidad , t01.lote ,t05.fecha_vencimiento \n" +
				"from  detalle_salida t01 \n" +
				"                                inner join  salidas  t02 on (t02.id=t01.id_salida)\n" +
				"                                inner join clientes  t03 on (t03.id=t02.id_cliente)\n" +
				"                                inner join productos t04 on (t04.id=t01.id_producto)\n" +
				"                                inner join entradas  t05 on (t05.lote=t01.lote)" +
				"                                inner join proveedor t06 on (t06.id=t04.id_proveedor) WHERE ";

		//Log.i("mostrar salida ",consulta+query);
		Cursor cursor = conexOpen.rawQuery(consulta+query, null);

		//Verificamos la existencia de datos en la consulta
		if(cursor.moveToFirst()){
			ReporteSalidas row = new ReporteSalidas();
			//Colocamos como fecha actual la prinera fecha
			String currentFecha=cursor.getString(0);
			while (cursor.isAfterLast()==false) {

				if(currentFecha.compareTo(cursor.getString(0))==0){
					row.id 					= cursor.getString(0);
					row.cliente				= cursor.getString(1);
					row.proveedor		    = cursor.getString(2);
					row.nombre_producto		= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.lote 				= cursor.getString(5);
					row.fechavencimiento	= cursor.getString(6);

				}else{
					//lo agregamos al listado de filas del reporte A
					mFilas.add(row);
					//Creamos un nuevo objeto de fila
					row = new ReporteSalidas();

					//colocamos los datos en el nuevo objeto
					row.id 					= cursor.getString(0);
					row.cliente				= cursor.getString(1);
					row.proveedor		    = cursor.getString(2);
					row.nombre_producto		= cursor.getString(3);
					row.cantidad 			= cursor.getString(4);
					row.lote 				= cursor.getString(5);
					row.fechavencimiento	= cursor.getString(6);
				}
				//Nos movemos al siguiente registro
				cursor.moveToNext();
			}
			//Agregamos el ultimo objeto de fila creado
			mFilas.add(row);
		}
		//Cerramos la conexio de la base datos
		//closeDatabase();
		desConectarDB();
		return mFilas;
	}



}//termina la clase